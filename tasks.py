from atelier.invlib import setup_from_tasks
ns = setup_from_tasks(
    globals(),
    use_dirhtml=True,
    tolerate_sphinx_warnings=False,
    languages=["et", 'en', 'ru'],
    revision_control_system='git',
    selectable_languages=('et','en','ru'),
    cleanable_files=[
        'docs/rss_entry_fragments/*'],
    intersphinx_urls=dict(docs="https://www.laudate.ee"))
