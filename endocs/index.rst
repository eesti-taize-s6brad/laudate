.. image:: /../docs/.static/20210807_165825.jpg
  :width: 90%

Welcome to the website of the Association of Estonian Taizé friends.

..
  Yes, the European Meeting 2024-25 was in Tallinn!
  :doc:`Read more <tallinn2024-25/index>`.

..
  This year's Taizé European Meeting is going right now in Ljubljana. The
  evening prayers are being broadcast live at 19:00 (Estonian time) on December
  28 and at 20:00 on December 29., 30. and 31.

  The place of next year's meeting was proclaimed during the prayer on 30.
  December!

  - Prayer broadcasts: https://www.taize.fr/en_article37386.html
  - What is the European Meeting: https://www.taize.fr/en_article35747.html



.. sidebar::

  .. rubric:: Weekly prayers in Estonia

  === ===== ======== ==================================
  Mon 18:00 Tallinn  :doc:`St. Charles' church <kaarli>`
  Wed 19:00 Tartu    :doc:`St. Luke's church <luuka>`
  Thu 17:30 Tartu    :doc:`St. Paul's church <pauluse>`
  Fri 19:00 Tallinn  :doc:`Mustamäe church <mmmk>`
  === ===== ======== ==================================

  See also: :doc:`all regular prayers <prayers>`.

.. rubric:: Latest blog entries

.. blogger_latest::

Home
====

.. .. raw:: html

  <script type="text/javascript" src="https://www.taize.fr/tz_readings.php?lang=en"></script>


.. toctree::
   :maxdepth: 1

   welcome
   tallinn2024-25/index
   prayers
   music
   taize
   about
   more

.. toctree::
   :hidden:

   reisid
