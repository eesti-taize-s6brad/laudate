=============
Peeteli kirik
=============

.. raw:: html

  <a title="Rene Seeman, CC BY-SA 3.0 EE &lt;https://creativecommons.org/licenses/by-sa/3.0/ee/deed.en&gt;, via Wikimedia Commons"
    href="https://commons.wikimedia.org/wiki/File:Peeteli_kirik.JPG"><img
    alt="Peeteli kirik"
    width="30%" align="right" style="padding-left:1em"
    src="https://upload.wikimedia.org/wikipedia/commons/thumb/4/46/Peeteli_kirik.JPG/256px-Peeteli_kirik.JPG"/></a>

| Ühispalvus Taizé lauludega
| iga kuu kolmandal kolmapäeval kell 18:00.

Asukoht: Preesi 5

Korraldaja :
`EELK Tallinna Peeteli kogudus <http://www.peetelikogudus.ee>`__

Bussipeatused:

- `Härjapea (Ristiku tn) <https://transport.tallinn.ee/#stop/10007-1,10008-1/map>`__
  40 59
- `Kolde puiestee (Kolde pst) <https://transport.tallinn.ee/#stop/10003-1>`__
  40
- `Ädala (Sõle tn) <https://transport.tallinn.ee/#stop/10101-1,10002-1/map>`__
  26 26A 32 33 48 66 72
