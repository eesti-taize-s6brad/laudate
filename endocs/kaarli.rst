=====================
St. Charles' church
=====================

.. .. note::
  In **December 2024** the prayers will happen at another place because the
  church is used for concerts.
  On Monday 2 December we will be in the **chapel of
  the Dominicans convent, Müürivahe 33**.
  On Monday 9 December we will be in **Allika kogudus, Koskla tn 18**.
  On Monday 16 December again in the **chapel of the Dominicans convent, Müürivahe 33**.
  On 24 and 31 December you will be at home with your family or with the European Meeting.
  And on January 6 we meet again in St. Charles' church.

.. sidebar::

    .. raw:: html

      <p align="center"><a title="Saint Charles church, Tallinn, Estonia, 2022-04"
      href="https://commons.wikimedia.org/wiki/File:Tallinn_asv2022-04_img12_StCharles_Church.jpg"><img
      height="240"
      alt="Saint Charles church, Tallinn, Estonia, 2022-04"
      src="https://upload.wikimedia.org/wikipedia/commons/thumb/f/f9/Tallinn_asv2022-04_img12_StCharles_Church.jpg/256px-Tallinn_asv2022-04_img12_StCharles_Church.jpg"></a>
      <small><small>(Foto: A.Savin, FAL, Wikimedia Commonsi kaudu)</small></small>
      </p>


:ref:`Common prayers <common_prayer>` every Monday at 6pm, song practice
before prayer at 5pm.

Place: `Tallinna Kaarli kirik
<https://et.wikipedia.org/wiki/Tallinna_Kaarli_kirik>`__

Organizer: `EELK Tallinna Kaarli kogudus <https://www.kaarlikogudus.ee/>`__

Contact: Annely Neame (5267825)

.. Muusikalised juhid: Luc (56672435) ja Hanna-Liisa
