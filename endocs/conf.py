# -*- coding: utf-8 -*-
from pathlib import Path
docs = Path('../docs').resolve()
fn = docs / 'conf.py'
with open(fn, "rb") as fd:
    exec(compile(fd.read(), fn, 'exec'))

# html_static_path = [str(docs / '.static')]
# templates_path = [str(docs / '.templates')]

language = "en"

rst_prolog = """
:doc:`Home </index>` |
:doc:`Prayers </prayers>` |
:doc:`/music` |
:doc:`Taizé </taize>` |
:doc:`/more` |
:doc:`/about`

"""

# :doc:`Meeting 2024/25 </tallinn2024-25/index>` |
