===========================
NÕO St. Lawrence church
===========================

Palvused reeglina iga kuu kolmandal reedel kell 19:30

Asukoht: `Nõo Püha Laurentsiuse kirik <https://et.wikipedia.org/wiki/N%C3%B5o_kirik>`__ (Tartu tänav 2, Nõo alevik, 61601 Nõo vald, Tartumaa)

Kontakt: õpetaja Mart Jaanson, tel 5661 1443, mart.jaanson@eelk.ee

Vaata ka http://nookirik.ee/teated/
