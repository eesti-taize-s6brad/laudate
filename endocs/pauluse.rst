===================
Tartu Pauluse kirik
===================

Palvus Taizé lauludega Pauluse kiriku krüptisaalis igal neljapäeval kell 17:30.

Asukoht : Riia 27, Tartu

Korraldaja: http://tartupauluse.ee/

Lisainfo: Andrus Mõttus, 5045352, andrus.mottus@eelk.ee
