========
Links
========

- Taizé community website: https://www.taize.fr

- Facebook pages `Ühispalvus Taizé lauludega  <https://www.facebook.com/coming.together.to.pray>`__
  and `Taizé sõbrad Eestis <https://www.facebook.com/Taiz%C3%A9-s%C3%B5brad-Eestis-482267422541366/>`__,
  and group `Taizé rändurid Eestist <https://www.facebook.com/groups/401405446661929/>`__.

- `Books about Taizé in Estonian
  <https://www.taize.fr/et_article10844.html?territ=27&category=1&lang=et>`__
  
