====================================
How to help maintaining this website
====================================

Please test the workflow below with a small change before investing much time in
bigger editorial work.

- Every page has a "Source code" link (the last two words in the footer). Click
  on this link and sign in on GitLab.

- Ask the webmaster to invite you as a maintainer of the repository.
  (:doc:`How to contact the webmaster? <about>`).

- Estonian pages are in the :file:`docs` directory, English translations are in
  and :file:`endocs` Russian translations in :file:`rudocs`.

- The menu bar is defined in the :file:`conf.py` file of each language
  (`ET <https://gitlab.com/eesti-taize-s6brad/laudate/-/blob/master/docs/conf.py>`__,
  `EN <https://gitlab.com/eesti-taize-s6brad/laudate/-/blob/master/endocs/conf.py>`__
  or `RU <https://gitlab.com/eesti-taize-s6brad/laudate/-/blob/master/rudocs/conf.py>`__)

- Currently the webmaster needs to pull your changes and build the pages on his
  computer before they get published.

- The software used to generate this website is `Sphinx
  <https://www.sphinx-doc.org>`__. Sphinx is the standard documentation system
  in the Python world.

- The markup language is called `reStructuredText
  <https://www.sphinx-doc.org/en/master/usage/restructuredtext/index.html>`__.
  It is a bit less intuitive than for example Markdown, but unlike Markdown it
  is designed for being extensible.
