.. raw:: html

  <img width="30%"
  alt="Usalduse palverännak Eestis" align="right"
  src="https://www.laudate.ee/data/2021/IMG_20210710_230449.jpg">

==================
Prayers in Estonia
==================

.. contents::
   :depth: 1
   :local:

Where to go?
============

Regular :ref:`common prayers <common_prayer>` are organized at different places
in Estonia.

- TALLINN :
  :doc:`kaarli` /
  :doc:`mmmk` /
  :doc:`peeteli` /
  :doc:`pyhavaimu`

- TARTU:
  :doc:`pauluse` / :doc:`luuka`

- ELSEWHERE IN ESTONIA:
  :doc:`hiiumaal` /
  :doc:`keila` /
  :doc:`kasmu` /
  :doc:`noo` /
  :doc:`rapina` /
  :doc:`saku`


.. toctree::
   :hidden:

   mmmk
   jaani
   peeteli
   ristiku
   pauluse
   pyhavaimu
   luuka
   hiiumaal
   kaarli
   keila
   kasmu
   noo
   rapina
   saku

..
  See also `Regulaarsed palvused Taizé lauludega
  <https://www.taize.fr/et_article26818.html>`__ on the Taizé website.


..
  _calendar:
  Calendar
  Here is a Google calendar with Taizé meetings in Estonia.

  .. raw:: html

    <iframe src="https://calendar.google.com/calendar/embed?src=mgs1hqa30g6qm9cup3p2g4h09c%40group.calendar.google.com&ctz=Europe%2FTallinn"
    style="border: 0" width="100%" height="400" frameborder="0" scrolling="no"></iframe>

  .. |add_google|  image:: /../docs/add_calendar.png
    :width: 100px

  How to integrate this calendar into your own Google calendar:
  https://support.google.com/calendar/answer/37100

.. _common_prayer:

What is a common prayer?
========================

.. sidebar::

  We do not need to wait for all the theological questions to be harmonized in
  order to come together to pray. When we meet with Christians of different
  denominations, (...) [we can] begin again and again by
  praying together. Such a practice of unity will allow God’s people to head
  towards a common confession of faith. -- br. Alois (`Letter 2023 <https://www.taize.fr/en_article35126.html>`__)

A **common prayer** is a prayer meeting held in the spirit and liturgical style
developed by the :doc:`Taizé community <taize>`. A typical prayer lasts 45
minutes, most of this time is filled with singing. Central elements are a bible
reading and a longer moment of deep silence.

A common prayer is inter-confessional, which means that we avoid elements that
are specific to a given confession. We try to make sure that everybody can
participate and feel at home, including people who are not used to liturgical
prayer.

Children are especially welcome and have a central place during prayer. Parents
should contact the organizer before the prayer in order to arrange individual
solutions.

The songs used during a common prayer are simple, meditative repetitions of
short texts from the Bible or early Christian authors. The participants have
both scores and lyrics so that everybody can join singing.
