=========================
St. John's church Tallinn
=========================

.. note::

  In September 2023 the congregation of St. John's church stopped hosting common
  prayers and the group searched for a new home. Prayers and rehearsals continue
  in :doc:`St. Charles' church <kaarli>`.

.. raw:: html

  <a title="Diego Delso, CC BY-SA 3.0
  &lt;https://creativecommons.org/licenses/by-sa/3.0&gt;, via Wikimedia Commons"
  href="https://commons.wikimedia.org/wiki/File:Iglesia_de_San_Juan,_Tallin,_Estonia,_2012-08-05,_DD_01.JPG"><img
  width="30%" align="right" style="padding-left:1em"
  alt="Iglesia de San Juan, Tallin, Estonia, 2012-08-05, DD 01"
  src="https://upload.wikimedia.org/wikipedia/commons/thumb/6/6f/Iglesia_de_San_Juan%2C_Tallin%2C_Estonia%2C_2012-08-05%2C_DD_01.JPG/256px-Iglesia_de_San_Juan%2C_Tallin%2C_Estonia%2C_2012-08-05%2C_DD_01.JPG"></a>


Common prayer every Monday at 18:00. Song rehearsal for those who wish from
16:30-17:30.

Place : Vabaduse väljak, Tallinn.

Every Monday at 18:00 in Jaani church (St. John's church), the yellow church
on *Vabaduse väljak*. From 16:30 to 17:30 we have a rehearsal for those who
wish to learn the songs.

Both the prayer and the rehearsal are usually held in the main church hall, but
it can happen that the hall is used by another event. On such days the prayer
happens in the cellar of the church. Enter through the little door on the left
side of the building.

Responsible pastor: Annely (5267825)

Musical directors: Luc (56672435) ja Hanna-Liisa

Organizer: `EELK Tallinna Jaani kogudus <https://www.tallinnajaani.ee/>`__
