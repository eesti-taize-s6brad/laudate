==============================
Mustamäe Mary Magdalene church
==============================

.. raw:: html

  <a title="Luc Saffre, CC BY-SA 4.0
  &lt;https://creativecommons.org/licenses/by-sa/4.0&gt;, via Wikimedia Commons"
  href="https://commons.wikimedia.org/wiki/File:Mustam%C3%A4e_Maarja_Magdaleena_kirik_(tagant,_Aprill_2021).jpg"><img
  width="40%" align="right"
  alt="Mustamäe Maarja Magdaleena kirik (tagant, Aprill 2021)"
  src="https://upload.wikimedia.org/wikipedia/commons/thumb/6/66/Mustam%C3%A4e_Maarja_Magdaleena_kirik_%28tagant%2C_Aprill_2021%29.jpg/512px-Mustam%C3%A4e_Maarja_Magdaleena_kirik_%28tagant%2C_Aprill_2021%29.jpg"></a>

Every Friday at 19:00 :ref:`common prayer <common_prayer>`
in Mustamäe Mary Magdalene church.


.. Enne palvust kell 17.30-18.30 :doc:`lauluklubi <lauluproovid>`.

Place : Kiili 9, 13423 Tallinn (`map
<https://kaart.delfi.ee/?bookmark=abe0fbff928478ee1525388d7aebd8e4>`__)

Organizer: `EELK Mustamäe Maarja Magdaleena kogudus <https://mustamaekogudus.weebly.com/>`__

Contact: Kaimo Klement (Tel 5219834)

Nearby bus stops:

- `Sipelga (A.H.Tammsaare tee) <https://transport.tallinn.ee/#stop/02111-1,03206-1/map>`__
  12 37
- `Siili (Sõpruse pst) <https://transport.tallinn.ee/#stop/02301-1,02302-1/map>`__
  11 24 24A 28 72 34 10 71 63
- `Mustamäe tee (A. H. Tammsaare tee) <https://transport.tallinn.ee/#stop/02109-1,02303-1/map>`__
  12 37
- `Sääse (Mustamäe tee) <https://transport.tallinn.ee/#stop/02201-1,02107-1/map>`__
  9 26 26A  1 5
