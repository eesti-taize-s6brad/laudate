======================
Tartu Püha Luuka kirik
======================

Õhtupalvused Taizé lauludega reeglina kolmapäeviti kell 17:00

Asukoht : `Püha Luuka kirik <https://et.wikipedia.org/wiki/Tartu_P%C3%BCha_Luuka_kirik>`__ (Vallikraavi 16a, 51003 Tartu)

Korraldaja: https://tartu.metodistikirik.ee

Lisainfo: pastor Priit Tamm, 518 6274, tartu@metodistikirik.ee
