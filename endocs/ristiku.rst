==============================
With the Mother Teresa Sisters
==============================

.. raw:: html

  <a title="Luc Saffre, CC BY-SA 3.0
  &lt;https://creativecommons.org/licenses/by-sa/3.0&gt;, via Wikimedia Commons"
  href="https://commons.wikimedia.org/wiki/File:Iglesia_de_San_Juan,_Tallin,_Estonia,_2012-08-05,_DD_01.JPG"><img
  width="30%" align="right" style="padding-left:1em"
  alt="Convent of the Missionaries of Charity in Tallinn, 2021-06-18"
  src="https://upload.wikimedia.org/wikipedia/commons/thumb/2/2a/Halastuse_Misjon%C3%A4ride_klooster_Tallinnas.jpg/320px-Halastuse_Misjon%C3%A4ride_klooster_Tallinnas.jpg"></a>

The `Missionaries of Charity
<http://www.katoliku.ee/index.php/en/information/1035-missionaries-of-charity-mother-teresa-sisters>`__
("Mother Teresa Sisters") in Tallinn would like to hold weekly :ref:`common prayers <common_prayer>` on
Tuesdays after the shared meal (around 14:00) in their soup kitchen. Primary
target would be the people who came for meal.  We need a second person who is
ready to commit for a weekly service and able to lead a Taizé prayer.

Place: Ristiku 44, 10320 Tallinn

Contact: 56672435 (Luc)
