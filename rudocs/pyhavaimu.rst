=========================
Church of the Holy Spirit
=========================

.. grid:: 1 2 2 2

  .. grid-item::

    Common prayer on the last Tuesday of every month at 7pm.

    Place: `Pühavaimu 2, Tallinn <https://en.wikipedia.org/wiki/Church_of_the_Holy_Spirit,_Tallinn>`__

    Organizaer: https://www.puhavaimu.ee/

    More information: 646 4430 (kantselei)

  .. grid-item::

    .. raw:: html

      <p align="center"><a title="Holy Spirit church, Tallinn, Estonia, 2022-04"
      href="https://commons.wikimedia.org/wiki/File:Tallinn_asv2022-04_img74_Holy_Spirit_Church.jpg"><img
      height="240"
      alt="Saint Charles church, Tallinn, Estonia, 2022-04"
      src="https://upload.wikimedia.org/wikipedia/commons/thumb/c/c3/Tallinn_asv2022-04_img74_Holy_Spirit_Church.jpg/256px-Tallinn_asv2022-04_img74_Holy_Spirit_Church.jpg"></a></p>

    (Photo: A.Savin, FAL, via Wikimedia Commonsi)
