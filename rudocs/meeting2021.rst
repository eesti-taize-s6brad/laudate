===================
Online meeting 2021
===================


Tuesday 28 December 20.30
=========================

Evening prayer from Taizé

.. youtube:: https://www.youtube.com/watch?v=yoXorappakQ

Wednesday 29 December Morning
=============================

Bible reflection. Brother Githinji speaks to us about Luke 10:25-37, in relation with the first
proposal of the `Message 2022 of brother Alois
<https://www.taize.fr/en_article32847.html>`__

.. youtube:: https://www.youtube.com/watch?v=K6At7zDR2TY&t=65s

Questions for a sharing:

1. Who is my neighbour?

2. Have I been faced by a situation where I was incapable/unable to lend a
   helping hand? What would make me hesitate whilst faced with a situation
   where someone is in deed of help?

3. What examples/testimonies of good neighbourliness/our belonging to each
   other, have I experienced/come across? - in my own life, where I live, etc.
   Where/how could I participate in a concrete way the life of another person in my
   day to day life?


Wednesday 29 December 20.30
===========================

evening prayer from Turin

.. youtube:: https://www.youtube.com/watch?v=_Vwu4MyZgrs&t=7s

Thursday 30 December morning
============================

Bible reflection


Thursday 30 December 12.30
==========================

midday prayer from Turin

Thursday 30 December 15.15
==========================

Live workshop broadcast from Turin: "I was a stranger and you welcomed me":
standing in solidarity with the world of migrants

Thursday 30 December 17.30
==========================

Live workshop broadcast from Turin: "Acting together to take care of our common home".

Thursday 30 December 8.30pm
===========================

evening prayer from Turin

Friday 31 December morning
==========================

Bible reflection

Friday 31 December 20.30pm
==========================

evening prayer from Taizé
