# -*- coding: utf-8 -*-
from pathlib import Path
docs = Path('../docs').resolve()
fn = docs / 'conf.py'
with open(fn, "rb") as fd:
    exec(compile(fd.read(), fn, 'exec'))

# html_static_path = [str(docs / '.static')]
# templates_path = [str(docs / '.templates')]

language = "ru"

rst_prolog = """
:doc:`Главная </index>` |
:doc:`Молитвы </prayers>` |
:doc:`/music` |
:doc:`Тэзе </taize>` |
:doc:`/p/index` |
:doc:`Встреча 2024/25 </tallinn2024-25/index>` |
:doc:`/links` |
:doc:`/about`

"""
