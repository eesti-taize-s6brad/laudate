=====
Music
=====

If you are a choir singer or a musician, then you can help with your skills to
make the prayers more beautiful.

You can learn the songs on this website:
https://www.taize.fr/en_article10308.html

We usually sing in Estonian during prayer, but the songs have been translated to
many other languages as well.  If you let the organizer know where you are from
*before* the prayer, they maybe decide to sing one of the songs in your
language.

`Song texts in Ukrainian (2 pages A4)
<https://www.laudate.ee/data/laulutekstid_ukr.pdf>`__
