==============
What is Taizé?
==============

Taizé is a small village in France, where a community of about 100 Christian
munks lives. More than 100,000 young people on pilgrimage visit them each year.
The meditative prayer songs from Taizé are just a side effect of this.


.. contents::
   :depth: 1
   :local:


Community
==========

.. raw:: html

  <a title="Foto: Damir Jelic, CC BY-SA 3.0, via Wikimedia Commons"
  href="https://commons.wikimedia.org/wiki/File:Taize1.JPG"><img width="30%"
  alt="Taizé küla Prantsusmaal" align="right"
  src="https://upload.wikimedia.org/wikipedia/commons/thumb/d/d3/Taize1.JPG/512px-Taize1.JPG"></a>

Taizé vennaskonda inspireerib kaks eesmärki: elada osaduses Jumalaga, ning olla
rahu ja usalduse kandjaks inimeste keskel. Vennaskond ei võta annetusi vastu,
vaid elatub oma käte tööst.


Prayer
======

.. raw:: html

  <a title="Foto: Christian Pulfrich, CC BY-SA 4.0 via Wikimedia Commons"
  href="https://commons.wikimedia.org/wiki/File:Mittagsgebet_in_der_Vers%C3%B6hnungskirche_in_Taiz%C3%A9.jpg"><img
  width="30%" align="right"
  alt="Lõunapalvus Lepituse Kirikus Taizés"
  src="https://upload.wikimedia.org/wikipedia/commons/thumb/7/7a/Mittagsgebet_in_der_Vers%C3%B6hnungskirche_in_Taiz%C3%A9.jpg/512px-Mittagsgebet_in_der_Vers%C3%B6hnungskirche_in_Taiz%C3%A9.jpg"></a>


Taizé ven­nas­konnas arenenud lihtne, meditatiivne ja muusi­kaline palvestiil
rõhutab elemente, mis on kristlastele ühised
ja väldib elemente, mis on spetsiifilised ühele konfessioonile.
See on algselt mõeldud noortele inimestele, kes ei ole harjunud
liturgiaga. Vaata täpsemalt :ref:`common_prayer`.

.. Palvestiil on samal ajal sügav ja madala sisenemislävega.

  .. "easy to enter", ("deep") ("accessible") spirituality



.. Muidugi on ka lapsed ja vanemad inimesed teretulnud.

Miks inimesed tulevad kokku ja palvetavad? Kuidas peaksime seda mõistma?
Üks religioonifilosoof kirjutas juba 100 aastat tagasi: "Liturgia on mäng. Püha
mäng, kus meie hing õpib "aega raiskama Jumala jaoks". Selle mängu mõte ei ole,
et me saavutaksime "midagi kasulikku". Siin ei ole tegemist maagilise
nõiavalemiga või salajase riitusega, vaid me osaleme selles mängus vabana, ilu
otsides ja pühas rõõmsameelsuses." -- (`Romano Guardini
<https://en.wikipedia.org/wiki/Romano_Guardini>`__, `Vom Geist der Liturgie
<https://openlibrary.org/works/OL1239319W/Vom_Geist_der_Liturgie?edition=>`__)


Spirituality
============

.. raw:: html

  <a title="Foto: Michael König, GNU-FDL/CC-by-SA, via Wikimedia Commons"
  href="https://commons.wikimedia.org/wiki/File:Mk_Fr%C3%A8re_Roger.jpg"><img
  width="30%" align="right"
  alt="vend Roger aastavahetusel 1991-92 Budapestis."
  src="https://upload.wikimedia.org/wikipedia/commons/thumb/0/0b/Mk_Fr%C3%A8re_Roger.jpg/367px-Mk_Fr%C3%A8re_Roger.jpg"></a>


Taizés kogevad inimesed Jumala ligiolu ja seal peetakse piiblitunde, kus vennad
seletavad, kuidas nemad piiblist aru saavad.

Taizé vennaskond on oikumeeniline, s.t. mitte seotud ühe konkreetse
konfessiooniga.

Õhkkond Taizés on mitmekeelne, mitmekultuuriline ja üksteist austav,
keskendub lihtsatele lahendustele.
See võimaldab kogeda kiriku avarust ("experience the wideness of the church").

Ühispalvus Taizé lauludega on võimalus kokku saada teiste kogudustega
oikumeenilises vaimus, s.t. keskendudes sellele, mis meid sõltumata
konfessioonist ühendab: meie igatsus Kristuse järele.

Kõik sellel kodulehel mainitud palvused on osa Taizé vennaskonna poolt välja
kuulutatud "usalduse palverännakust maailmas".  Usalduse palverännak on esmalt
kohtumine - ülestõusnud Kristuse ning teistega. Tänu ühistele palvehetkedele
muudame me end Jumala jaoks kättesaadavaks. Jagamise ja külalislahkuse kaudu on
kõik nõus ületama barjääre ning erinevusi, et üksteist vastu võtta ning
rikastada. Vend Roger' jaoks oli esmatähtis mitte organiseerida liikumist Taizé
vennaskonna ümber. Vastupidi, pärast usalduse palverännakul osalemist on igaüks
kutsutud minema koju ning väljendama oma eluga seda, mida tema on evangeeliumist
mõistnud. Igaüks peaks ka olema enam teadlik oma sisemisest elust ning
praktilistest solidaarsuse tegudest, mida ta saaks oma lähiümbruses rakendada.
Paljudes riikides tulevad noored regulaarselt kokku ning peavad palvusi Taizé
lauludega, jäädes samas seotuks oma kohaliku kirikuga. (Rohkem selle kohta saad
lugeda `siit <https://www.taize.fr/et_article736.html>`__).



Pilgrimage of Trust
====================

.. raw:: html

  <a title="Usalduse palverännak"
  href="https://www.taize.fr"><img width="30%"
  alt="Usalduse palverännak" align="right"
  src="https://www.laudate.ee/data/pilgrimage_of_trust.png"></a>


Taizé vennaskond ei ole *liikumine*. Nad ei korralda teiste inimeste elusid.
Küll aga annavad juhtnööre neile, kes soovivad oma kodukohas nende vaimsusega
ühineda.

Iga kord, kui mõni kogudus korraldab üritus Taizé vaimsuses, võtab
ta selle kaudu osa **usalduse palverännakust**.
