=========================================
Welcome to Tallinn for 2024/25
=========================================

Taizé friends in Estonia are looking forward to receive the 2024/25 European
Meeting in Tallinn.
We see this project as a great gift to our people and our country.
The event is organized by the Taizé community in collaboration with the
`Estonian Council of Churches <http://ekn.ee/english.php>`__.



.. image:: ../../docs/tallinn2024-25/logo_Tallinn_colour.jpg
  :width: 60%
  :align: right

What if you planned to enter the year 2025 far from the glittery, in silence and
in prayer for peace, with young people from all over Europe?

Do you know that a family is opening its doors for you on the shores of the
Baltic Sea at the end of the year?

.. Will you dare to approach the "Digital Tiger"?


..
  Announcements
  - Short: https://www.youtube.com/shorts/11kQPhNARi4
  - Longer: https://youtube.com/watch?v=AR2Lvk-KlSM&si=Zmgf7G2hYtsBIFyq
  - Full: After the `evening prayer in Ljubljana on December 30 <https://www.youtube.com/watch?v=n2ePjSAlhPg>`__ (starting at 1:03:40)


Newsticker
==========

- 2023-12-30 (en, et, de, fr) https://www.taize.fr/en_article37442.html
- 2023-12-30 (et) https://e-kirik.eelk.ee/2023/jargmine-euroopa-noorte-aastavahetuse-kokkutulek-toimub-tallinnas/
- 2023-12-31 (de) https://www.vaticannews.va/de/welt/news/2023-12/naechstes-taize-jugendtreffen-ende-2024-tallinn-estland.html


Provisional program
===================

.. sidebar::

  "The pilgrimage is an important event for those who undertake it, for those who
  dare to set out and run the risk of failure, but also for those who welcome
  others, and, by doing so, receive more than they give. Thousands of young
  pilgrims join thousands of people who agree to open their homes to complete
  strangers. They share more, far more, than their goods; they share their lives,
  their beliefs, their hopes."
  -- participant of a previous meeting


.. rubric:: Saturday 28/12/2024

- Arrival and welcome of participants in local communities, hospitality in families
- Supper and evening prayer at Tondiraba ice hall

.. rubric:: Sunday 29/12, Monday 30/12 Tuesday 31/12

- Morning prayer and program in the host communities
- Midday prayer
- Afternoon workshops by team in Tallinn city center
- Supper at Tondiraba ice hall (a packed lunch is given for the following day)
- Prayer together at Tondiraba ice hall

.. rubric:: Wednesday 01/01/2025

- Morning prayer and farewell meeting in the host communities
- Lunch in families
- Departure for the journey back home


See also the `Taizé website <https://www.taize.fr/en_rubrique3863.html>`__.
