========
Contact
========

This website is maintained by the *Association of Estonian Taizé Friends*.

Our association has no accounting of its own, it does not organize any events,
it's purpose is to inform about the ideas behind the Taizé community and about
the pilgrimage of trust in Estonia.

Questions and feedback are welcome to

- webmaster webmaster@laudate.ee (Luc Saffre)
- board members: juhatus@laudate.ee (Tiina Klement, Annely Neame & Luc Saffre)
