=====================
Welcome to foreigners
=====================

Foreigners of all confessions are warmly welcome to the common
inter-confessional prayers with songs from Taizé held on a regular basis in
Estonia.

Taizé-style prayers consist mostly of singing, plus one short Bible reading, a
time of silence and a time of free intercessions.  If your mother tongue is not
Estonian, we invite you to check with the organizer of the prayer before the
meeting so that you may read it in your preferred language.

See also :doc:`prayers`

..
  Try to not arrive in the last minute. Call +372 56672435 (Luc) in case you get
  lost (but of course I mute my phone during prayer).
