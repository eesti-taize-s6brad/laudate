=====================
St. Charles' church
=====================


.. grid:: 1 2 2 2

  .. grid-item::

    Common prayers every Monday at 6pm.

    Place: `Tallinna Kaarli kirik
    <https://et.wikipedia.org/wiki/Tallinna_Kaarli_kirik>`__

    Organizer: `EELK Tallinna Kaarli kogudus <https://www.kaarlikogudus.ee/>`__

    Contact: Annely Neame (5267825)

    .. Muusikalised juhid: Luc (56672435) ja Hanna-Liisa

  .. grid-item::

    .. raw:: html

      <p align="center"><a title="Saint Charles church, Tallinn, Estonia, 2022-04"
      href="https://commons.wikimedia.org/wiki/File:Tallinn_asv2022-04_img12_StCharles_Church.jpg"><img
      height="240"
      alt="Saint Charles church, Tallinn, Estonia, 2022-04"
      src="https://upload.wikimedia.org/wikipedia/commons/thumb/f/f9/Tallinn_asv2022-04_img12_StCharles_Church.jpg/256px-Tallinn_asv2022-04_img12_StCharles_Church.jpg"></a></p>

    (Photo: A.Savin, FAL, via Wikimedia Commonsi)



Enne palvust kell 16:30-17:30 lauluproov neile, kes soovivad.
Ootame koori- ja soololauljaid juurde.

Istu julgesti koos koorilauljatega altariruumi.
Me kõik oleme Jumala külalised, keegi ei ole suur juht ja õpetaja.
Igaüks on oluline.
