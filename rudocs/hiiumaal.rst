======================
HIIUMAAL Kassari kabel
======================

Palvused Taizé lauludega iga kuu viimasel laupäeval kell 18:00.

Asukoht: `Kassari kabel <https://et.wikipedia.org/wiki/Kassari_kabel>`__

Kontakt: Triinu Pihel, tel. 56907373, triinuplix@gmail.com

.. Diakon Triin Simson, tel. 56212357, simsontriin@gmail.com


Vaata ka https://dago.ee/kirik/kaina/
