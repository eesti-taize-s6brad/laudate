=========
Üritused
=========

.. toctree::
   :maxdepth: 1

Eesolevad üritused
==================

.. toctree::
   :maxdepth: 1

   /p/2024/0809


..
  Reisid Taizésse

  - 25.03.-01.04.2024 Läheme Taizésse ülestõusmispühadeks. Sõidame 25. märtsil,
    esmaspäeval. Tagasi 1. aprillil, ka esmaspäeval. Hommikul vara Brüsselisse,
    sealt Lyoni. Kohal u 11-12 vahel päeval. Tagasi Lufthansaga, vahepeatus
    Frankfurdis. Tallinnas mõned mitutid peale südaööd. Kui soovid liituda, siis
    helista 56646029 (Ülle)

  - Suvel korraldab Kaarli kogudus reis Taizésse. Igaüks on teretulnud liituda.
    Kontakt: meelis.holsting@kaarlikool.ee


Möödunud üritused
==================

.. toctree::
   :maxdepth: 1

   /p/2023/1226
   /p/2023/0930
   /p/2023/0811
   /p/2023/0720
   /p/2023/0618
   /p/2023/0415
   /p/2022/1228
   /p/2022/0812
   /p/2021/0910
   /p/2021/0821
   /p/2021/0813
   /p/2021/0806
   /p/2021/0710
   /p/20201128
   /p/20200807
   /p/2017/0102


.. toctree::
   :hidden:

   2021/index
   2022/index
   2023/index
   2024/index
