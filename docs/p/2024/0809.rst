================================================
9.--11.08.24 : Taizé sõbrad Viru Folgil
================================================

.. sigal_image:: taize/2022/08/tv/1e.jpg|thumb|Eesti Taizé sõbrad Viru Folgil

9.--11. august 2024 toimub Käsmu külas (Lääne-Virumaal) traditsiooniline
festival "Viru Folk". Viru Folk toob igal aastal Käsmu üle kümne tuhande
inimese. Kolme päeva jooksul toimuvad kontserdid hommikust õhtuni üksteise
järele (`programm <https://www.virufolk.ee/virufolk2024-programm/>`__).

Juba viiendat korda kutsub Käsmu kogudus sel nädalavahetusel Taizé sõpru Eesti
erinevatest paikadest kokku, et festivali sees elada koos :ref:`usalduse
palverännaku <usalduse_palverännak>` vaimus. Nii kombineerime enda jaoks Viru
Folgi hinguse ja Taizé vaimsuse. Me osaleme festivali sündmustel ja lisaks
sellele kutsume kolm korda päevas festivali külastajaid meiega liituma
ühispalvusel kirikus.

Kui Sulle meeldib laulda, siis kutsume Sind osaleda. Kutsume Sind eriti siis kui
"kirik" on Sulle võõras. Viru Folgil saad järgi proovida oma nahal, kuidas Jumal
räägib Sinuga keset festivali möllu. Usutunnistust pole vaja, ainus tingimus on
laululust ja sinu kohalolek kogu nädalavahetusel.  Ainult ära viivita
registreerimisega, sest kohad on piiratud!

.. Iga osaleja aitab kaasa, et palvused oleksid ilusad.


:ref:`Ühispalvus <common_prayer>` on Prantsusmaal Taizé vennaskonnas arenenud
palvestiil. Palvus kestab umbes 45 minutit, mille ajal on peamine väljendusviis
laulmine. Jutluse asemel on lühike lugemine Piiblist ja umbes 10 minutit
vaikust. Laulude tekstid on üherealised ja lihtsad meditatsioonid, mida
korratakse kümneid kordi nagu mantraid. Lastel on eriline roll palvustel. Nad
istuvad esikohas ja võivad palvuse ajal vaikselt mängida, joonistada ning olla
lihtsalt lapsed. Nad tuletavad meile meelde Jeesuse sõnad "Laske lapsed minu
juurde tulla, ärge keelake neid, sest selliste päralt on Jumala riik!"

..
  Osalemine ei eelda kindlat usutunnistust, vaid eesmärk on, et kõik saaksid
  kogeda Jumala ligiolu, kaasa arvatud need, kellel on vähe kogemust "liturgiaga".


Vaata ka :doc:`möödunud aastate fotosid </virufolk>`.

Programm
========

Reede 9. august:

- 16.00 Saabumine kiriku juures (ööbimiskohad, kiriku ettevalmistus)
- 18.00 Kogunemine Tia juures (kohvilaud, tutvustusmäng)
- **19.00 Avatud lauluproov** kirikus
- **20.00 Ühispalvus Taizé lauludega** kirikus

Laupäev 10. august:

- 8.00 Hommikupalvus värskes õhus algab kiriku ees
- **10.00 Avatud lauluproov** kirikus
- **11.00 Ühispalvus Taizé lauludega** kirikus
- 14.00 Töötoad ja vestlused kirikus
- **20.00 Ühispalvus Taizé lauludega** kirikus

Pühapäev 11. august

- 8.00 Hommikupalvus värskes õhus algab kiriku ees
- **10.00 Avatud lauluproov** kirikus
- **11.00 Ühispalvus Taizé lauludega** kirikus
- 15.00 Koguduse missa kirikus


Palun **registreeri ennast kuni 31. juulini** telefonil 58141366 (Tia) või
e-maili tiamariav@gmail.com kaudu. Registreerimisel täpsustad oma soove ööbimise
ja söömise kohta: tasuta ööbimisvõimalus oma telgis.  Sööki teeme koos lihtsatel
tingimustel.

Osalejatele
===========

Vaata :doc:`0809a`

Käsmu küla ja Viru Folk üldiselt
================================


Rohkem info Käsmu kohta leiad siit:

- `EELK Käsmu koguduse FB koduleht <https://www.facebook.com/eelkkasmu>`__.
- `MTÜ Viru Folk <https://www.virufolk.ee/>`__
- `Vikipeedia <https://et.wikipedia.org/wiki/K%C3%A4smu>`__
- `MTÜ Käsmu külaselts <http://kasmu.eu/>`__
- `Laane Pansion <http://www.laanepansion.ee/est/Toitlustamine-3>`__
- `Lainela puhkeküla <https://lainela.ee/>`__


.. raw:: html

  <p><a href="https://commons.wikimedia.org/wiki/File:K%C3%A4smu_kirik3.jpg#/media/Fail:Käsmu_kirik3.jpg"><img
  src="https://upload.wikimedia.org/wikipedia/commons/8/8c/K%C3%A4smu_kirik3.jpg"
  alt="Käsmu kirik3.jpg" width="324" height="243"></a>
  <br>
  Käsmu kirik (<a href="//commons.wikimedia.org/wiki/User:Mona-Mia" title="User:Mona-Mia">Mona-Mia</a> – <span class="int-own-work" lang="et">Üleslaadija oma töö</span>, <a href="https://creativecommons.org/licenses/by/3.0"
  title="Creative Commons Attribution 3.0">CC BY 3.0</a>,
  <a href="https://commons.wikimedia.org/w/index.php?curid=10844724">Link</a>)
  </p>

  <p><a
  href="https://commons.wikimedia.org/wiki/File:K%C3%A4smu_laht.jpg#/media/Fail:Käsmu_laht.jpg"><img
  src="https://upload.wikimedia.org/wikipedia/commons/0/02/K%C3%A4smu_laht.jpg"
  alt="Käsmu laht.jpg" width="320" height="214"></a><br>
  Käsmu laht (<a
  href="//commons.wikimedia.org/wiki/User:Aleksander_Kaasik"
  title="User:Aleksander Kaasik">Aleksander Kaasik</a> – <span
  class="int-own-work" lang="et">Üleslaadija oma töö</span>, <a
  href="https://creativecommons.org/licenses/by-sa/3.0" title="Creative Commons
  Attribution-Share Alike 3.0">CC BY-SA 3.0</a>, <a
  href="https://commons.wikimedia.org/w/index.php?curid=29936541">Link</a>)</p>



.. Infotahvel kiriku juures: Koroona piirangute tõttu ei toimu sel aastal
   kontserte Käsmu kirikus.  Selle asemel toimuvad siin Eesti Taizé Sõprade
   palvused, piiblitunnid, proovid ja vestlused. Meie programm on avatud festivali
   külastajale, kes tunneb "teeks väikest pausi ja prooviks midagi rahulikumat".
   Sissepääs on tasuta. Sa oled meie külaline, palun käitu viisakalt ja pea
   hügieeninõuetest kinni.


.. toctree::
   :hidden:

   0809a
