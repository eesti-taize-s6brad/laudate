====
2023
====

..
  Sel suvel on mitu võimalust kokku saada teiste Taizé sõpradega ja külastada
  erinevaid paiku Eestis. Otsime veel lauljaid ja muusikuid, kes tuleksid appi.


.. toctree::
   :maxdepth: 1

   0415
   0618
   0720
   0811
   0930
