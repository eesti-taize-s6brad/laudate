..
  Vaata ka :doc:`/uudised`, :doc:`blogi </blog/2022/index>`
  ja `RSS uudistevoog <https://laudate.ee/rss.xml>`_.

..
  Osa meist kuulub ka Facebookigruppi `Taizé rändurid Eestist
  <https://www.facebook.com/groups/401405446661929/>`__.
