=====================
Tallinna Kaarli kirik
=====================

.. .. note::
  Detsembris 2024 Kaarli kirikus palvusi esmaspäeviti **ei ole**, sest kirikus
  toimuvad kontsertid. Selle asemel palvetame esmaspäeval, **2. detsembril**
  Dominiiklaste kabelis (Müürivahe 33). Ka proov kell 17 sealsamas.
  **9. detsembril** kohtume Allika koguduse ruumides, Koskla tn 18.
  **16. detsembril** kohtume jälle Dominiiklaste kabelis (Müürivahe 33).
  24. ja 31. detsembril oleme oma peredes või Euroopa Kokkutulekul.
  Ja **6. jaanuaril 2025** läheb elu edasi Kaarli kirikus.

.. sidebar::

    .. raw:: html

      <p align="center"><a
      title="Saint Charles church, Tallinn, Estonia, 2022-04"
      href="https://commons.wikimedia.org/wiki/File:Tallinn_asv2022-04_img12_StCharles_Church.jpg"><img
      width="100%"
      alt="Saint Charles church, Tallinn, Estonia, 2022-04"
      src="https://upload.wikimedia.org/wikipedia/commons/thumb/f/f9/Tallinn_asv2022-04_img12_StCharles_Church.jpg/256px-Tallinn_asv2022-04_img12_StCharles_Church.jpg"></a>
      <small><small>(Foto: A.Savin, FAL, Wikimedia Commonsi kaudu)</small></small>
      </p>


:ref:`Ühispalvus <common_prayer>` igal esmaspäeval kell 18:00.
Enne palvust kell 17:00 lauluproov ja ettevalmistused.
Igaüks on teretulnud! Ootame lauljaid juurde!

Asukoht: `Tallinna Kaarli kirik
<https://et.wikipedia.org/wiki/Tallinna_Kaarli_kirik>`__

Korraldaja: `EELK Tallinna Kaarli kogudus <https://www.kaarlikogudus.ee/>`__

Kontakt: Annely Neame (5267825)


..
  Istu julgesti koos koorilauljatega altariruumi.
  Me kõik oleme Jumala külalised, keegi ei ole suur juht ja õpetaja.
  Igaüks on oluline.

Galerii
=======

.. image:: /data/2024/IMG_20240108_181353.jpg
  :width: 90%

..
  .. raw:: html

    <video width="240" height="320" controls>
     <source src="/data/2024/20240115_180159.mp4" type="video/mp4">
    Your browser does not support the video tag.
    </video>
