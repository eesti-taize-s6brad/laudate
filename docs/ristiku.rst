=======================
Ema Teresa Õdede juures
=======================

.. raw:: html

  <a title="Luc Saffre, CC BY-SA 3.0
  &lt;https://creativecommons.org/licenses/by-sa/3.0&gt;, via Wikimedia Commons"
  href="https://commons.wikimedia.org/wiki/File:Iglesia_de_San_Juan,_Tallin,_Estonia,_2012-08-05,_DD_01.JPG"><img
  width="30%" align="right" style="padding-left:1em"
  alt="Convent of the Missionaries of Charity in Tallinn, 2021-06-18"
  src="https://upload.wikimedia.org/wikipedia/commons/thumb/2/2a/Halastuse_Misjon%C3%A4ride_klooster_Tallinnas.jpg/320px-Halastuse_Misjon%C3%A4ride_klooster_Tallinnas.jpg"></a>

The `Missionaries of Charity
<http://www.katoliku.ee/index.php/en/information/1035-missionaries-of-charity-mother-teresa-sisters>`__
("Mother Teresa Sisters") in Tallinn held monthly :ref:`common prayers
<common_prayer>` until 2022. Right now there are no such prayers here. 

Koht: Ristiku 44, 10320 Tallinn

.. Kontakt: 56672435 (Luc)

..
  Palvus Taizé lauludega iga kuu kolmandal reedel kell 16:30.

  Asukoht: Ristiku 44, 10320 Tallinn

  Korraldaja : `Missionaries of Charity. Mother Teresa Sisters
  <http://www.katoliku.ee/index.php/en/information/1035-missionaries-of-charity-mother-teresa-sisters>`__
