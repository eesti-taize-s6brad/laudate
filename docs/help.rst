==========================================
Kuidas kaasa aidata veebilehe toimetamisel
==========================================

Kui sul on GitLabi konto, siis võid sa otse hakkata parandama, näiteks
käesoleva lehe `lähtekood on siin
<https://gitlab.com/eesti-taize-s6brad/laudate/-/blob/master/docs/help.rst>`__.

GitLab on programmeerijatele mõeldud ja see vajab võib-olla veidi harjumist, aga
see pole raketiteadus ja kui sa soovid, siis saad veebiredaktorilt tasuta
koolituse :-)

Täpsemad juhtnöörid on inglise keeles: https://www.laudate.ee/en/help/
