================================
HAAPSALU Jaani kirik
================================

Palvused Taizé lauludega iga kuu viimasel laupäeval kell 18.00.

Lähikuudel siis 26.oktoobril ja erandkorras 23.novembril 2024.

Asukoht : `Haapsalu Jaani kirik <https://et.wikipedia.org/wiki/Haapsalu_Jaani_kirik>`__

Korraldaja: `EELK Haapsalu Püha Johannese kogudus <https://haapsalu.eelk.ee/>`__

Kontakt:
