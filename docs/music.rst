=======
Muusika
=======

Kui oled laulja või mängid mõnda pilli, siis võid sa oma oskustega aidata, et
palvused kõlaksid ilusti. Muusiku eesmärk palvusel ei ole esineda vaid olla
abiks teistele külastajatele.

Mõned failid sellel veebisaidil on salasõnaga kaitsud, sest autoriõigus keelab
neid avalikustada. Kui soovid neid kasutada, siis küsi salasõna mõne Eesti Taizé
sõbra käest.

Kui oskad muusikat noodistada: ootame kedagi, kes viiks Luc'i alustatud
kogumistöö edasi. Kõik MuseScore faile saad tema käest küsida.

..
  Mõnikord toimub lauluproov enne palvust. See on võimalus kokku saada inimestega,
  kes tahavad Taizé muusikat paremini tundma õppima. See on ka võimalus kaasa
  mõelda ja arutada, kuidas palvus saaks muusikalisel tasemel edasi areneda.

.. contents::
  :local:


Koorilauljatele
================

Kõik Taizé laulud (originaalkeeltes) saab õppida vennaskonna kodulehelt:
https://www.taize.fr/et_article10315.html

**Eestikeelne Taizé laulik** sai välja antud 2009. aastal nelja-aastase töö
tulemusena (`kuulutus
<https://www.eestikirik.ee/valmib-eestikeelne-taize-laulik/>`__). Seda saab osta
LNÜ kontorist, Kiriku plats 3, 10130, Tallinn. Lauliku hinnaks on 5€. Ostes 10
on ühe hinnaks 4€ ja ostes 100 on tüki hinnaks 3€. Postiga tellimiseks saatke
oma soov lny@eelk.ee. Laulik on saadaval ka `Logose poes
<https://www.logos.ee/toode/taize-laulud/>`__ ja `Tartus Pauluse raamatupoes
<https://pauluseraamatupood.ee/toode/taize-laulud-2009/>`__.
Sõbrad saavad seda ka `pdf failina alla laadida </dl/taize_laulud.pdf>`__.

**Ukrainakeelsed laulutekstid** `printimiseks A4
<https://www.laudate.ee/data/laulutekstid_ukr.pdf>`__



..
  `tiitelleht </dl/taize-lauludA5-210x148-tiitelleht.pdf>`__ /
  `kaaned </dl/taize-lauludA5-420x148-5mm-bleed-1.pdf>`__ /
  `lähtekood </dl/taize.zip>`__ (pole prindimiseks vaja)

..
  Mõned failid sellel lehel saab alla laadida ainult salasõnaga, sest neid
  ei tohi `autoriõiguse tõttu <https://www.taize.fr/et_article1092.html>`__
  niisama jagada. Anna teada, kui tahad neid alla laadida ja kaasa lüüa!


Lauliku lisa
============

Uued Taizé laulud ootavad tõlkimist. Plaan on selline, et millalgi anname välja
pdf fail nende uute lauludega, mida iga kogudus saab ise välja printida ja oma
laulikutele lisada. Natuke meie töö tulemusest on juba näha:

- https://www.laudate.ee/dl/laulud2/taize_laulud_2.pdf

..
  Millalgi teeme ka **tõlketalgud**, kus me uusi laule katsetame.
  Töö käib **meililisti kaudu**. Kui tahad kaasa lüüa, siis liitu selle listiga:

    https://groups.google.com/g/taize-t6lkijad/about

  Liituda saab ka siis, kui sa ei soovi luua Google kontot. Sel juhul võta meiega
  ühendust.


Soololauljatele
===============

Mõned soolopartiid on tõlgitud eesti keelde.

- Luc'i soolode kogu : `pdf </dl/soolod/Soolod.pdf>`__ / `odt </dl/soolod/Soolod.odt>`__

Tundmatu tõlkija (sulgudes on eestikeelse lauliku numbrid):

- (4) Kus on halastus, armastus / Ubi caritas Deus ibi est:
  `jpg 1 </dl/soolod/ubi_caritas_deus_ibi_est1.jpg>`__ /
  `jpg 2 </dl/soolod/ubi_caritas_deus_ibi_est2.jpg>`__
  (`youtube <https://www.youtube.com/watch?v=TQzQEsRbDU4>`__)
- (17) Minu Jumal / El Senyor: `jpg </dl/soolod/el_senyor.jpg>`__
  (`youtube <https://www.youtube.com/watch?v=N_UksBJyHMY>`__)
- (41) Magnificat (choral): `jpg </dl/soolod/magnificat.jpg>`__
  (`youtube <https://www.youtube.com/watch?v=hhLYCLS09h8>`__)
- (46) Sind usaldame / In te confido: `jpg </dl/soolod/in_te_confido.jpg>`__
  (`youtube <https://www.youtube.com/watch?v=tfjnVa1WPZ8>`__)
- (136) Issand, minust kõike sa tead / Viešpatie tu viską žinai: `jpg </dl/soolod/viespatie.jpg>`__
  (`youtube <https://www.youtube.com/watch?v=Hiiekf-H-WE>`__)

Pillimängijatele
================

Terved kogumikud ja noodiraamatud saab `Taizé veebipoest
<https://shop.taize.fr/en/taxons/songbooks>`__.
Paljud instrumentaalpartiid saab osta
`Exultet veebipoest <https://www.exultet-solutions.com/shop/pages-main/partner_id-10/index.html>`__.
Osa neist on keegi juba ostnud
ja me jagame neid siin Eesti Taizé sõprade kasutamiseks (küsi salasõna mõne
Eesti Taizé sõbra käest):

- (127) Behüte mich Gott : `instrumental </dl/instrumental/Behute_mich_Gott_ins.pdf>`__ (lihtne viis, flööt, oboe, b-klarnet)
- Adsumus Sancte Spiritus : `instrumental </dl/instrumental/Adsumus_ins.pdf>`__ (flööt, oboe, tšello või fagott)
- Jubelt und freut euch : `instrumental </dl/instrumental/Jubelt_und_freut_euch_ins.pdf>`__ (lihtne viis, flööt, klarnet, trompett, sarv)
- Herre visa mig vägen : `instrumental </dl/instrumental/Herre_visa_mig_vagen_guitar.pdf>`__ (kitarr)
- Sanasi on lamppu : `instrumental </dl/instrumental/Sanasi_on_lamppu_ins.pdf>`__ (klaver, kitarr, oboe, klarnet)
- Let all who are thirsty come :
  `instrumental </dl/instrumental/Let_all_who_are_thirsty_come_ins.pdf>`__ (klaver, kitarr, flööt, oboe, klarnet, trompett, tšello või fagott)
- Voici Dieu qui vient :
  `instrumental </dl/instrumental/Voici_Dieu_qui_vient_ins.pdf>`__ (klaver, kitarr, lihtne viis, b-klarnet, flööt, oboe, f-sarv, tšello või fagott)
  / `kitarr </dl/instrumental/Voici_Dieu_qui_vient_Guitare.pdf>`__
