================
Noortekohtumised
================

.. note:: See text on alles töötamisel. Ootan sinu tagasiside.

Alates 2020. aastast hakkavad Tallinnas vist toimuma igakuised kohtumised, kuhu
me kutsume noori inimesi vanuses 13-30, kes küsivad: kuidas me saame elada
selles linnas, selles riigis ja sellel planeedil nii, et see oleks täiuslik ja
jätkusuutlik?

.. image:: /data/2020/nk-kutse.png
  :width: 40%
  :align: right

Esimene kokkutulek on **(kuupäev ja koht otsustamata)**.
Esimene päevateema on **Tehnoloogia, inimene ja Jumal**

Sissejuhatuseks räägib keegi külaline umbes 10 minutit. Pärast seda jagavad
noored ennast väikestesse grupidesse (5-10 inimest) ja valivad endale
vestlusjuhi.  Sissejuhatuse lõpus anti küsimusi, mis võivad aidata vestlust
alustada. Variandid:

- Vestluste sisu jääb rühmaliikmete teada.

- Vestluse lõpus kirjutab iga grupp mõni eestpalvee, mis sõnastab nende mured.
  Eestpalveid toome soovi korral ühispalvuse ajal Jumala ette.

- Vestlusjuhid edastavad oma grupi mõtteid videotiimile, kes mõtleb neid edasi
  ja loob hiljem videofilmi selle materjali põhjal.

Ajakava (näide)

- Kell 17.00 : sissejuhatus
- Kell 17.30 : vestlused rühmades
- Kell 18.30 : tee ja suupisted
- Kell 19.30 : ühispalvus
- Kell 20.30 : pidu

.. Osalejate arv on piiratud! Registreeri ennast viivimatult helistades
  53730718 (Tiina),
  56726863 (Maris) või
  56672435 (Luc).

.. NB! Me kutsume kooliõpetajaid seda kutset levitama.  Küsimustele vastavad
  korraldajad hea meelega. Õppeväljundid : Osalejad saavad aru, et igaühe arvamus
  on oluline ja väärtuslik. Osalejad õpivad, et tänulik ja opimistlik
  ellusuhtumine on võimalik ka rasketes olukordades. Osalejad  harjutavad, mida
  see hoiak konkreetselt kaasa toob teiste inimestega suhtlemisel.
