=======
Ajalugu
=======

2024
====

- 2024-12-12 `Ringvaade: Usulaager <https://jupiter.err.ee/1609549828/usulaager>`__
- 2024-11-16 `Vikerraadio: Mart Normet. Taizé noortekohtumine Tallinnas <https://vikerraadio.err.ee/1609523246/mart-normet-taiz-noortekohtumine-tallinnas>`__

2020
====

30. juuli -- 12. august 2020 :
Mustamäe EELK kogudus sõitis ühe auto ja kahe minibussiga Taizésse.
Osalejad olid 10 noort ja 8 täiskasvaut.
Taizés kohapeal olid nad terve nädal ehk 2.-9. august.
Reisijuht oli Tiina Klement.
Reisi maksumus oli ligikaudu 320 eurot.
Koroonaviiruse tõttu viibisid osalejad pärast reisi veel 2 nädalat karantiinis.


18. november 2020:
Intervjuu Eesti Kiriku ajalehes:
`Luc Saffre: Inimesed igatsevad Jumala järele
<http://www.eestikirik.ee/luc-saffre-inimesed-igatsevad-jumala-jarele/>`_


2019
====

Novembris 2019 sõitis Tuhala Kaarli kogudus Taizésse.
Raadio 7 saates `Mõttekoda 20.11.2019
<https://raadio7.ee/podcast/mottekoda-2019-11-20/>`__ räägivad
Kalev Kask (Juuru Mihkli kogudusest), Margus Suumann (Tuhala Kaarli kogudusest) ja Kairi Tamjärv (Tapa Jakobi kogudusest),
mida nad seal kogesid.


**5. juulil** pärast tantsupidu ja laulupeo proovide lõppu kell 23-24ni olid
kõik lauljad-tantsijad ja teisedki kutsutud osalema oikumeenilisel Taizé
palvusel Tallinna Jaani kirikus. `Kutse Tallinna Jaani kirikusse Taize palvusele
<http://www.eestikirik.ee/kutse-tallinna-jaani-kirikusse-taize-palvusele/>`__


2017
====

:doc:`/p/2017/0102`.


2016
====

`Intervjuu vend Philipiga Taizé kloostrist
<https://www.youtube.com/watch?v=fF_zRdInK8g>`__ --
Aprillikuus 2016 Eestit külastanud Taizé munk vend Philip räägib, kuidas Taizé
kogukond Lõuna-Prantsusmaal alguse sai, kuidas see on nii suureks kasvanud
tänapäeval ja miks Taizé palvused on nii teistsuguse vormiga. Veel saame teada,
miks Philip isiklikult oma elus selle tee on valinud ja mida ta ütleb noortele,
kes võitlevad raskustega.

2012
====

Vend John külastas Eestit keset talve:
`Renewing trust <https://www.taize.fr/en_article13669.html>`__
/ `Vertrauen erneuern <https://www.taize.fr/de_article13673.html>`__ /

2011
====

Maikuus tulid külla kaks vabatahtlikut Taizést, Monika ja Christina:
`Visiting the Christians <https://www.taize.fr/en_article12583.html>`__
/ `Sich als Christen besuchen <https://www.taize.fr/de_article12612.html>`__ /

2010
====

Veebruaris tuli vend John külla:

`Visits in February 2010 <https://www.taize.fr/en_article8624.html>`__
/ `Besuche im Februar 2010 <https://www.taize.fr/de_article9859.html>`__

2009
====

- 18.03.2009 ilmus Eesti Kirikus artikkel `Valmib eestikeelne Taizé laulik
  <https://www.eestikirik.ee/valmib-eestikeelne-taize-laulik/>`__ (Autor Kristel Põder).

- 29.11.2009 Advendipalvus Nõmme Lunastaja kirikus


2008
====

`In Latvia and Estonia: 2008
<https://www.taize.fr/en_article7735.html>`__
