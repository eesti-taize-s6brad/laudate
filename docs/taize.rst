=============
Mis on Taizé?
=============

Taizé on väike küla Prantsusmaal, kus elab umbes 100 munka. Enam kui 100.000
inimest lähevad neile külla igal aastal. Taizés arenenud lihtsad meditatiivsed
laulud on sellest vaid kõrvalmõju.


.. contents::
   :depth: 1
   :local:


Vennaskond
==========

.. raw:: html

  <a title="Foto: Damir Jelic, CC BY-SA 3.0, via Wikimedia Commons"
  href="https://commons.wikimedia.org/wiki/File:Taize1.JPG"><img width="30%"
  alt="Taizé küla Prantsusmaal" align="right"
  src="https://upload.wikimedia.org/wikipedia/commons/thumb/d/d3/Taize1.JPG/512px-Taize1.JPG"></a>


Taizé vennaskonda inspireerib kaks eesmärki: elada osaduses Jumalaga, ning olla
rahu ja usalduse kandjaks inimeste keskel. Vennaskond ei võta annetusi vastu,
vaid elatub oma käte tööst.


Palvus
======

.. raw:: html

  <a title="Foto: Christian Pulfrich, CC BY-SA 4.0 via Wikimedia Commons"
  href="https://commons.wikimedia.org/wiki/File:Mittagsgebet_in_der_Vers%C3%B6hnungskirche_in_Taiz%C3%A9.jpg"><img
  width="30%" align="right"
  alt="Lõunapalvus Lepituse Kirikus Taizés"
  src="https://upload.wikimedia.org/wikipedia/commons/thumb/7/7a/Mittagsgebet_in_der_Vers%C3%B6hnungskirche_in_Taiz%C3%A9.jpg/512px-Mittagsgebet_in_der_Vers%C3%B6hnungskirche_in_Taiz%C3%A9.jpg"></a>


Taizé ven­nas­konnas arenenud lihtne, meditatiivne ja muusi­kaline palvestiil
rõhutab elemente, mis on kristlastele ühised
ja väldib elemente, mis on spetsiifilised ühele konfessioonile.
See on algselt mõeldud noortele inimestele, kes ei ole harjunud
liturgiaga. Vaata täpsemalt :ref:`common_prayer`.

.. Palvestiil on samal ajal sügav ja madala sisenemislävega.

  .. "easy to enter", ("deep") ("accessible") spirituality



.. Muidugi on ka lapsed ja vanemad inimesed teretulnud.

Miks inimesed tulevad kokku ja palvetavad? Kuidas peaksime seda mõistma?
Üks religioonifilosoof kirjutas juba 100 aastat tagasi: "Liturgia on mäng. Püha
mäng, kus meie hing õpib "aega raiskama Jumala jaoks". Selle mängu mõte ei ole,
et me saavutaksime "midagi kasulikku". Siin ei ole tegemist maagilise
nõiavalemiga või salajase riitusega, vaid me osaleme selles mängus vabana, ilu
otsides ja pühas rõõmsameelsuses." -- (`Romano Guardini
<https://en.wikipedia.org/wiki/Romano_Guardini>`__, `Vom Geist der Liturgie
<https://openlibrary.org/works/OL1239319W/Vom_Geist_der_Liturgie?edition=>`__)


Vaimsus
=======

.. raw:: html

  <a title="Foto: Michael König, GNU-FDL/CC-by-SA, via Wikimedia Commons"
  href="https://commons.wikimedia.org/wiki/File:Mk_Fr%C3%A8re_Roger.jpg"><img
  width="30%" align="right"
  alt="vend Roger aastavahetusel 1991-92 Budapestis."
  src="https://upload.wikimedia.org/wikipedia/commons/thumb/0/0b/Mk_Fr%C3%A8re_Roger.jpg/367px-Mk_Fr%C3%A8re_Roger.jpg"></a>


Taizés kogevad inimesed Jumala ligiolu ja seal peetakse piiblitunde, kus vennad
seletavad, kuidas nemad piiblist aru saavad.

Taizé vennaskond on oikumeeniline, s.t. mitte seotud ühe konkreetse
konfessiooniga.

Õhkkond Taizés on mitmekeelne, mitmekultuuriline ja üksteist austav,
keskendub lihtsatele lahendustele.
See võimaldab kogeda kiriku avarust ("experience the wideness of the church").

Ühispalvus Taizé lauludega on võimalus kokku saada teiste kogudustega
oikumeenilises vaimus, s.t. keskendudes sellele, mis meid sõltumata
konfessioonist ühendab: meie igatsus Kristuse järele.



.. _usalduse_palverännak:

Usalduse palverännak
====================

.. raw:: html

  <a title="Usalduse palverännak Eestis"
  href="https://www.laudate.ee"><img width="30%"
  alt="Usalduse palverännak Eestis" align="right"
  src="https://www.laudate.ee/data/pilgrimage_of_trust.png"></a>

**Usalduse palverännak** on nimi, mille Taizé vennaskond annab oma pikaajalisele
projektile. Iga kord, kui mõni kogudus korraldab üritus "Taizé vaimsuses", võtab
ta selle kaudu osa **usalduse palverännakust**.

Usalduse palverännak on esmalt kohtumine - ülestõusnud Kristuse ning teistega.
Tänu ühistele palvehetkedele muudame me end Jumala jaoks kättesaadavaks.
Jagamise ja külalislahkuse kaudu on kõik nõus ületama barjääre ning erinevusi,
et üksteist vastu võtta ning rikastada.

.. sidebar::

  "Nende noorte inimeste siseelu on meile oluline.  Ilma, et me neile peale
  tükiksime, igatseme seda, et nad ei peaks tundma üksinduse ega Jumala
  puudumise.  Jumal ei jäta kedagi üksi, ta suudab ainult armastada. Kuidas
  peaksime neile seda ütlema nii, et me neid austame ning ei solva nende
  mõtlemist?" -- vend Roger


..
  Taizé - Dokumentation von 2004
  https://youtube.com/watch?v=vsLRXCTX3ks&si=kidDntn7_zIoGFHy

  Frère Roger ab ca Minute 33: "Es geht um das innere Leben dieser jungen
  Menschen. Ohne uns ihnen aufzudrängen besteht unsere Sehnsucht darin, dass sie
  sich nicht der Einsamkeit ausgeliefert fühlen müssen und damit der Abwesenheit
  Gottes. Gott verlässt niemanden. Er kann nur lieben. Wie sollen wir ihnen das
  sagen und sie dabei respektieren und ihr Denken nicht vor den Kopf stoßen?"



.. Huvitaval kombel ei ole sellel küsimusel ametlik vastust!

Aga mida tähendab "Taizé vaimsus"?
Vend Roger' jaoks oli esmatähtis mitte organiseerida liikumist Taizé vennaskonna
ümber. Vastupidi, pärast usalduse palverännakul osalemist on igaüks kutsutud
minema koju ning väljendama oma eluga seda, mida tema on evangeeliumist
mõistnud. Igaüks peaks ka olema enam teadlik oma sisemisest elust ning
praktilistest solidaarsuse tegudest, mida ta saaks oma lähiümbruses rakendada.

Paljudes riikides tulevad noored regulaarselt kokku ning peavad palvusi Taizé
lauludega, jäädes samas seotuks oma kohaliku kirikuga. (Rohkem selle kohta saab
lugeda `siit <https://www.taize.fr/et_article736.html>`__).

Vennaskond jagab praktilisi juhtnööre neile, kes soovivad oma kodukohas
nende vaimsusega ühineda. Ja seda isegi eesti keeles: `Kuidas palvust ette
valmistada? <https://www.taize.fr/et_rubrique2770.html>`__.



Milleks minna Taizésse?
=======================

Taizé asub Tallinnast 2528 km kaugusel. Milleks sinna sõita? Siin on mõned
vastused.

Vennad vastavad: "Külaskäik Taizésse on võimalus otsida osadust Jumalaga palve,
laulu, vaikuse ja mõtiskluste kaudu. Siin on võimalik taas leida sisemine rahu,
elu mõte ja uus impulss eluks. Lihtsa ühiselu kogemine tuletab meile meelde, et
Kristus ootab meid just meie igapäevases elus."

Mõned reisjad vastavad:

Madara (16): Minu kogemus seoses Taizéga oli väga meeldiv. Tore oli aega
veeta teiste kristlike noorte ja Jumalaga. Taizés võlus mind ka kogu
sealne lihtsus. Ma arvan, et Taizé on imeline koht, kus olla ja kuhu minna.
Olen väga tänulik kogu selle reisi eest.

Jürgen (17): Taizé üks suur eelis ongi praktiline kogemus kristlikust
osadusest. Eestis on võimalik seda kogemust ka kirikus mitte kunagi saada.
Loomulikult pole tavaelus koguduses võimalik nädalaks välismaailmast eemale
tõmbuda, kuid vähemalt teisi koguduseliikmeid tundma õppida ja üksteist toetada
ei keela meil keegi. Väga tihedalt koos tegutsemise puhul tekib paratamatult
eriarvamusi, purelemist ja muid probleeme. Kurb on aga see, kui isegi konflikte
ei saa tekkida, sest me lihtsalt ei tunne oma koguduses olevaid inimesi.

Mari (18): Taizésse tuleb tuhandeid noori igast maailma nurgast. Koha peal
toimuvad kolm korda päevas palvused (mis annavad tõelise Püha Vaimu laksu!),
piiblitunnid ja rahvusvahelised grupivestlused. Taizés on hea võimalus teiste
kristlastega kokku puutuda ja tutvusi terves maailmas sobitada! Taizé üks
võludest ongi just see, et seal nii suures hulgas noori koos on. Üheskoos
palvetades tekib omaette energia, mida tasub kogeda. Kujuta ette laulupidu, aga
kristlikult.

Kätlin (18): Reis andis sügava kogemuse ja eriliselt puudutas see, et seal on
kõik teretulnud. Kui meie kristlastena räägime tihti tingimusteta armastusest ja
ütleme, et hoolime kõigist, siis Taizé vennad näitavad seda igal tasandil
inimestele päriselt. Nad ei anna hinnanguid ja samas nad ei muuda enda
tõekspidamisi ega kontseptsiooni, selleks et teistele meeldida. Nad on endas
kindlad ja samas hinnanguvabad. Need on kaks olulist asja, mida meil kristlastel
tuleks õppida – me ei pea oma tõekspidamisi teiste pärast muutma (et olla
meeldivad), kuid peaksime inimese vastu võtma just seal, kus ta parasjagu oma
eluga on. Keegi ei tohiks tunda end väljaheidetuna, kuid samas on kogukonnas oma
tõekspidamised, millega inimene peab arvestama, et seal olla. See annab aga just
turvatunde ja rahulolu – võin olla see, kes ma olen, ja samas pean arvestama ka
teistega. Nii käitus minu meelest just Jeesus. Kokkuvõttes – kui inimesel
lubatakse olla turvalises keskkonnas tema ise oma looga ja haavatavusega, siis
enamasti inimene tahab olla hea ja muutuda. Kui me märkame inimeses seda HEAD ja
mitte ei raiska energiat sellele, et tema VIGU üles lugeda, saab juhtuda
armastuse ime! See ei tähenda aga seda, et see inimese halb pool oleks justkui
ükskõik, sellele lihtsalt ei anta nii palju energiat ja siis ta vaibub
iseenesest...

Luc (52): Nädal Taizés on nagu suurpuhastus, mis võimaldab heita pilk omaenda
ellu uue vaatenurga alt. Reisi ajal saad kokku huvitavate inimestega. Need, kes
sinna sõidavad, on väga erinevad, aga neid ühendab huvi Jumala vastu.


Mida tehakse Taizés?
====================

Loe `Mis toimub Taizés igapäevaselt?
<http://taize.fr/et_article14773.html>`__

Vaata vendade poolt tehtud sissejuhatus:

.. youtube:: https://www.youtube.com/watch?v=ngA8BFbjrE0

Vaata muljed lühifilmis `UK Schools prilgrimage 2018
<https://www.youtube.com/watch?v=qmnvOdUELH0>`__
"Experience Taizé, one of the most unique places on earth" produced by
The Archbishop of York Youth Trust:

.. youtube:: https://www.youtube.com/watch?v=qmnvOdUELH0

Vaata saksakeelset filmi ateistist, kes läks Taizésse:

.. youtube:: XGUz9dPihjE
