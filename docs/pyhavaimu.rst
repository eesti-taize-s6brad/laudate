=========================
Tallinna Püha Vaimu kirik
=========================

.. grid:: 1 2 2 2

  .. grid-item::

    Ühispalvus Taizé lauludega iga kuu viimasel teisipäeval kell 19:00.

    Asukoht: Pühavaimu 2, Tallinn 10123

    Korraldaja: https://www.puhavaimu.ee/

    Lisainfo: 646 4430 (kantselei)

  .. grid-item::

    .. raw:: html

      <p align="center"><a title="Holy Spirit church, Tallinn, Estonia, 2022-04"
      href="https://commons.wikimedia.org/wiki/File:Tallinn_asv2022-04_img74_Holy_Spirit_Church.jpg"><img
      height="240"
      alt="Saint Charles church, Tallinn, Estonia, 2022-04"
      src="https://upload.wikimedia.org/wikipedia/commons/thumb/c/c3/Tallinn_asv2022-04_img74_Holy_Spirit_Church.jpg/256px-Tallinn_asv2022-04_img74_Holy_Spirit_Church.jpg"></a></p>

    (Photo: A.Savin, FAL, via Wikimedia Commonsi)
