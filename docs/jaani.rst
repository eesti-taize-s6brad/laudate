====================
Tallinna Jaani kirik
====================


.. note::

  Septembris 2023 loobus Tallinna Jaani kogudus ühispalvuste korraldamisest
  resursside puuduse tõttu. Palvused ja lauluproovid toimuvad edasi 
  :doc:`Kaarli kirikus <kaarli>`.


.. grid:: 1 2 2 2

  .. grid-item::

    Palvus Taizé lauludega igal esmaspäeval kell 18.00.

    Asukoht: `Tallinna Jaani kirik
    <https://et.wikipedia.org/wiki/Tallinna_Jaani_kirik>`__ (Vabaduse väljak,
    Tallinn)

    Korraldaja: `EELK Tallinna Jaani kogudus <https://www.tallinnajaani.ee/>`__

    Kontakt: Õpetaja Annely Neame (5267825)

    .. Muusikalised juhid: Luc (56672435) ja Hanna-Liisa

  .. grid-item::

    .. raw:: html

      <p align="center"><a title="Saint John's church, Tallinn, Estonia, 2012-08-05"
      href="https://commons.wikimedia.org/wiki/File:Iglesia_de_San_Juan,_Tallin,_Estonia,_2012-08-05,_DD_01.JPG"><img
      height="240"
      alt="Saint John's church, Tallinn, Estonia, 2012-08-05, DD 01"
      src="https://upload.wikimedia.org/wikipedia/commons/thumb/6/6f/Iglesia_de_San_Juan%2C_Tallin%2C_Estonia%2C_2012-08-05%2C_DD_01.JPG/256px-Iglesia_de_San_Juan%2C_Tallin%2C_Estonia%2C_2012-08-05%2C_DD_01.JPG"></a></p>

    (Foto: Diego Delso via Wikimedia Commons, `CC BY-SA 3.0
    <https://creativecommons.org/licenses/by-sa/3.0>`__)


.. grid:: 1 2 2 2

  .. grid-item::

    Enne palvust kell 16:30-17:30 lauluproov neile, kes soovivad.
    Ootame koori- ja soololauljaid juurde!
    Mõnikord peame proovi peaukse esikus ja kutsume möödakäijaid ka laulma.

  .. grid-item::

    .. sigal_image:: taize/2022/07/img_20220725_165821.jpg|thumb|Lauluproov

  .. grid-item::

    Palvused ja lauluproov toimuvad tavaliselt kirikus, sissepääs peauksest.
    Kui kirik on kontserdi jaoks hõivatud, siis palvetame "katakombides", sissepääs
    keldriuksest kantselei ukse kõrval.

    Istu julgesti koos koorilauljatega altariruumi.
    Me kõik oleme Jumala külalised, keegi ei ole suur juht ja õpetaja.
    Igaüks on oluline.


  .. grid-item::

    .. sigal_image:: taize/2022/07/img_20220725_175829.jpg|thumb|Altariruumis
