:orphan:

===========
Autoriõigus
===========

Kõike, mida me siia kirjutasime, võid sa ilma luba küsimata kopeerida, ümber
muuta ja mujal uuesti avalikustada tingimusel, et sa oma allikat nimetad
("laudate.ee" või "Eesti Taizé Sõprade seltsing").

..
  Mõned failid sellel veebisaidil on salasõnaga kaitsud, sest  nende väljaandja on
  keegi teine ja nende kohta kehtivad nende autorite reeglid.
