=================
Piiblivestlused
=================

"Tahaksin oma sõpradega tõsiselt ja ausalt rääkida sellest, mis mind tegelikult
puudutab. Tahaksin rääkida teistega oma rõõmudest ja muredest ilma et peaksin
kartma, et teised naeravad mu üle."

Kui oled vahetevahel nii mõelnud, siis pakun sulle sellise eksperimendi.

Kuidas teha
===========

- Mõtle järgi, milliseid konkreetseid sõpru sa kutsuksid. Teid peaks olema
  vähemalt kolm inimest.

  Vaata, et osalejatel oleks enam-vähem sama suured kogemused Piibliga. Kui üks
  osaleja teab Piiblist tunduvalt rohkem kui teised, siis on oht, et temast
  areneb "guru" (mida tahame vältida).

- Leidke endale sobiv aeg ja koht.  Kõige lihtsam on käia koos kellelgi kodus.

- Vali välja mõni piibliteksti. Ideaalne pikkus on 10 kuni 20 salmi. Kui sa ei
  tea, kust alustada, siis võta näiteks see, mida Taizés täna loetakse:
  `Piiblilugemised igaks päevaks <https://www.taize.fr/et_article151.html>`__

- Igal osalejal peaks olema oma Piibel.  Kui paberipiibleid pole piisavalt, siis
  http://www.piibel.net käib ka.

- Valige endale juht selleks korraks. Järgmisel kohtumisel võiks olla keegi
  teine juhiks.

  Juht loeb lihtsalt ette, mis juhendil on kirjas. Juht hoolitseb selle eest, et
  igal osalejal oleks võimalus rääkida (kui ta tahab).  Muidugi ei pea kedagi
  sundima. Juht vaatab, millal võiks järgmisele punktile üle minna.

- `Juhend on siin </data/pj7sammuga.pdf>`__. See võiks olla iga osaleja käes.
  Need seitse punkti on juhtnöörid. Keegi ei kontrolli, kas te neid täpselt nii
  jälgite.

- Lülitage telefonid ja taustamuusikat välja ja hakkake pihta!


Märkused
========

- Selle meetodi abil kohtuvad inimesed üle maailma.
- Igaüks võib seda teha. Pole rohkem vaja kui kaks sõpra.
- Tore oleks, kui sa räägid ka mõne vaimulikuga, kuidas teil läks.
- Sama tore oleks, kui sa seda kuskil mujal jagad.
- Kui teie grupp kasvab suuremaks kui 8 inimest, siis mõelge jagunemisele:
  parem 2x4 kui 1x8.
