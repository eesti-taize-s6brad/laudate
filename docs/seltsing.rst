:orphan:

==================
Eesti Taizé Sõbrad
==================

.. contents::
  :local:

Eesmärk
=======

Seltsingu "Eesti Taizé Sõbrad" eesmärk on tutvustada Taizé vennaskonna
põhimõtteid ja Eestis toimuvaid Taizéga seotud tegevusi.

Seltsingu juhatus on Tiina Klement, Annely Neame ja Luc Saffre.
Seltsingu asutusliikmed on Pia Hanslep, Tiina Klement, Andrus Mõttus, Annely
Neame, Ülle Reimann ja Luc Saffre.
Veebiredaktor on Luc Saffre.

Seltsing loodi 2020. aastal juunikuus
(vaata ka :doc:`üleskutse </blog/2020/0527>` ja `asutusleping </data/ETSS.pdf>`__).

Seltsingu eesmärk **ei ole** korraldada üritusi ega reise.
Meie ainult *räägime* sellest, mida teised *teevad*. See on ka töö.
Näiteks veebilehe laudate.ee toimetamine,
erinevate tegijate vaheline suhtlus,
laulude tõlkimine eesti keelde,
plakatite ja muu dokumentide koostamine.

Me ei võta vastu rahalisi annetusi (seltsingul polegi raamatupidamist), meid
saab aindata ainult tegusid tehes. Igaüks meist aitab kaasa oma võimaluste
piires ja me kõik teeme seda vabatahtlikult.


Domeeninimi
===========

Meie domeeninimi on ``laudate.ee``,
Kaks Taizé laulu algavad selle sõnaga:
`Laudate Dominum <https://www.taize.fr/spip.php?page=chant&song=463&lang=et>`__
ja
`Laudate omnes gentes <https://www.taize.fr/spip.php?page=chant&song=464&lang=et>`__.

Mõlemad laulud tähendavad "Kiitke Issandat, kõik rahvad" ja see tekst tuleb
`psalmist 116
<https://www.biblestudytools.com/vul/psalms/116.html>`__, eestikeeles piiblis
`Psalm 117
<http://www.piibel.net/#q=ps%20117%3A1>`__.

Ja see ongi hea kokkuvõte sellest, mida Taizé vennaskond tahab: kiita Jumalat ja
kuulutada tema rõõmusõnumi kõikidele inimestele.


Mis on seltsing?
================

"Seltsing on mitteformaalsema iseloomuga kodanike vabatahtlik ühendus. Seltsing
ei ole juriidiline ega füüsiline isik ning teda ei kanta mittetulundusühingute
ja sihtasutuste registrisse. Tema asutamisel sõlmitakse küll liikmete vahel
seltsinguleping, kuid seda ei esitata äri- ja ühinguregistrile ning tasuma ei
pea riigilõivu." (`mtyabi.ee
<https://www.mtyabi.ee/nouanded/organisatsiooni-asutamine/seltsingu-asutamine/>`__).
Vaata ka `heakodanik.ee
<https://heakodanik.ee/kuidas-asutada-seltsingut/?fbclid=IwAR25zUWorWFXVDWq-TM8sUA6SsmXfRy-mF-54IRfb_a6CvvDRkcmPyIawm0>`__.

Me jälgime oma töös `Vabaühenduste Eetikakoodeksi
<https://heakodanik.ee/vabauhenduste-eetikakoodeks/>`__ põhimõtteid.

.. Peamine suhtlemine käib :doc:`töörühmades <lists>`.


Palveta meie eest
=================

Tore oleks, kui sa saaksid palvetada koos meiega, et Issand saadaks meile
abilisi ja annaks, et me võtaksime vastu uusi liikmeid suure südamega.

Veel toredam oleks, kui meie ka teame, et sa palvetad. Ütle seda välja mõnel
ühispalvusel. Või saada e-maili aadressile kirjutades paar julgustavat või ka
manitsevat sõna.
