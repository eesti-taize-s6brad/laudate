====================
SAKU Toomase kogudus
====================

Praegu on selgumisel, millal palvused Taizé lauludega jälle algavad. Huvi korral
võta ühendust õpetajaga, iga huviline loeb!


Asukoht :

- Palvemaja: Tallinna mnt 8, 75501 Saku
  `kaart.delfi.ee <https://kaart.delfi.ee/?bookmark=64aed242f55e33084f5855aaf4df7eb0>`__

- Kirik: Küütsu 2, 75501 Saku.
  `kaart.delfi.ee <https://kaart.delfi.ee/?bookmark=41e27ad5be9073179bff3d1c02cbe58d>`__

Korraldaja: `Saku Toomase kogudus <http://saku.eelk.ee/>`__

Lisainfo: Õpetaja Magne Mølster, magnemo29@gmail.com, 59197802
