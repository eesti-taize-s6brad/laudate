=========
Töörühmad
=========

.. toctree::
   :maxdepth: 1



..
  - noored@laudate.ee (noortetöötegijad ja kooliõpetajad)
  - muusikud@laudate.ee (juhid, pillimängijad, soololauljad)
  - reisijad@laudate.ee (reisid Taizésse ja ekskursioonid)
  - meedia@laudate.ee (ajakirjanikud ja meediatöötajad)

  NB: meililistid ei tööta veel, anna teada, kui arvad, et see oleks oluline.
