:date: 2020-06-29

=========================
Esmaspäev, 29. juuni 2020
=========================

Laul **Õndsaks kiitmised** on nüüd ka tõlkijate töölehes (tulevases
lisavihikus).  Vaata :doc:`/music`.
