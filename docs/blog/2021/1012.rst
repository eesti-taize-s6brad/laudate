:date: 2021-10-12

============================
Tallinnas ja Roomas
============================

12. oktoober 2021

**Tallinas**: Sel nädal on kolmas reede kuus, see tähendab, et palvus Mustamäe
kirikus jääb ära ja selle asemel palvetame ema Teresa õdede juures (ja seal
algab kell 16:30, mitte kl 19:00 nagu Mustamäe kirikus).

**Roomas**: Vend Alois rääkis möödunud laupäeval Roomas Piiskoppide Sinodi
avamisel (tema kõne algab 1:40):

    https://www.youtube.com/watch?v=NgANIW3Fm0g&t=3s

Kutsun kõiki Taizé sõpru tuleval pühapäeval kell 11:30 osaleda missal Peetruse
ja Pauli katedrali (Vene tn), et koos katoliiklastega pühitseda seda ajaloolist
hiigelprojekti ja meid julgustama. Mina olen Eestis üks eestvedajatest. Ma
hoiatan teid juba nüüd ette, et ka mittekatoliiklased on kutsutud osalema ja
sõna võtma sellel sinodil. Varsti algavad konsultatsioonid ja siis ma räägin
teile sellest rohkem.

Luc
