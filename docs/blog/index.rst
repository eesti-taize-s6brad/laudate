=========
Uudiskiri
=========

Aeg-ajalt saadame uudiskirja kõikidele, kes on meie meililistis.
Uudiskirjaga liitumiseks saada
meile lihtsalt e-kirja ja ütle, et soovid liituda uudiskirjaga.

..
  Kui sul on Google konto, siis mine aadressile
  https://groups.google.com/g/laudate-uudised/about
  ja vajuta nuppule "Liitu grupiga".

Kui tahad neid uudiseid oma telefonis lugeda, siis paigalda mõni RSS lugeja
(näiteks `Flym
<https://play.google.com/store/apps/details?id=net.frju.flym&hl=en>`_) ja
mine uudistevoogu linkile https://www.laudate.ee/rss.xml

Uudiskirjade arhiiv:

.. blogger_index::
