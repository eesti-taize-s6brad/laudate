====================
KEILA Miikaeli kirik
====================

.. raw:: html

  <a title="Lefevrue, CC BY-SA 3.0 &lt;https://creativecommons.org/licenses/by-sa/3.0&gt;, via Wikimedia Commons"
    href="https://commons.wikimedia.org/wiki/File:Keila_church.jpg"><img
    alt="Keila Miikaeli kirik"
    width="30%" align="right" style="padding-left:1em"
    src="https://upload.wikimedia.org/wikipedia/commons/thumb/9/94/Keila_church.jpg/256px-Keila_church.jpg"/></a>


| Palvus Taizé lauludega
| iga kuu viimasel reedel kell 19:00

Asukoht : `Keila Miikaeli kirik <https://et.wikipedia.org/wiki/Keila_kirik>`__, Keskväljak 1, Keila

Korraldaja: `EELK Keila Miikaeli kogudus <https://www.keilakirik.ee/kontakt/>`__

| Lisainfo: Õpetaja Matthias Burghardt
| tel 53405948
| e-post matthias.burghardt@eelk.ee
