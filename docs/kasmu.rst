===========
KÄSMU kirik
===========

Palvus Taizé lauludega *suveperioodil* igal laupäeval, september--mai iga kuu
viimasel laupäeval kell 18:00.

Asukoht : `Käsmu kirik <https://et.wikipedia.org/wiki/K%C3%A4smu_kirik>`__
(Laane tee 4, Käsmu küla, 45601 Haljala vald, Lääne-Virumaa)

Korraldaja: `EELK Käsmu kogudus <https://www.facebook.com/watch/eelkkasmu/>`__

Kontakt: Õpetaja Urmas Karileet, urmas.karileet@eelk.ee, tel 5851 5800
