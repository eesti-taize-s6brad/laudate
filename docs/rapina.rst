=======================
RÄPINA kirik
=======================

Palvused Taizé lauludega igal reedel kell 19.00.

Asukoht:
`Miikaeli kirik
<https://et.wikipedia.org/wiki/R%C3%A4pina_kirik>`__
(Võõpsu mnt 7, 64503 Räpina, Põlvamaa)

Korraldaja: `EELK Räpina Miikaeli kogudus <https://rapina.eelk.ee/>`__

Kontakt: Õpetaja Urmas Nagel, urmas.nagel@eelk.ee, 56158776
