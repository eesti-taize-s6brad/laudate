========
Lingid
========

- Taizé vennaskonna veebisait eesti keeles: https://www.taize.fr/et

- Facebookis on olemas
  leheküljed `Ühispalvus Taizé lauludega  <https://www.facebook.com/coming.together.to.pray>`__
  ja `Taizé sõbrad Eestis <https://www.facebook.com/Taiz%C3%A9-s%C3%B5brad-Eestis-482267422541366/>`__
  ning vestlusgrupp `Taizé rändurid Eestist <https://www.facebook.com/groups/401405446661929/>`__.

- `Raamatud
  <https://www.taize.fr/et_article10844.html?territ=27&category=1&lang=et>`__
  Taizé kohta eesti keeles.
