======================================
Taizé sõbrad Viru Folgil
======================================

Sel aastal: :doc:`/p/2024/0809`

Aastast 2015 korralab EELK Käsmu kogudus
:ref:`ühispalvuseid Taizé lauludega <common_prayer>`
Viru Folgi raames.
2020. aastal palus kogudus seltsingu abi nende palvuste korraldamisel.
Nii sai Viru Folgist traditsiooniline kokkutuleku võimalus Eesti Taizé Sõpradele.

Siin on möödunud aastate kutsed ja fotod:

- :doc:`/p/20200807` (`blogi <https://belglane.saffre-rumma.net/blog/2020/0809/>`__)
- :doc:`/p/2021/0806` (`blogi <https://belglane.saffre-rumma.net/blog/2021/0819/>`__)
- :doc:`/p/2022/0812` (`blogi <https://belglane.saffre-rumma.net/blog/2022/0816/>`__)
- :doc:`/p/2023/0811` (`blogi <https://belglane.saffre-rumma.net/blog/2023/0814/>`__),
