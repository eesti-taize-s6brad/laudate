==============================
LOKSA Püha Neitsi Maarja kirik
==============================

Õhtupalvused Taize lauludega neljapäeviti kell 19.00

Asukoht: `Loksa Püha Neitsi Maarja kirik <https://et.wikipedia.org/wiki/Loksa_kirik>`__,
Tallinna tn 54, Loksa, Harjumaa

Korraldaja: `Loksa Püha Neitsi Maarja kogudus <https://et.wikipedia.org/wiki/Loksa_P%C3%BCha_Neitsi_Maarja_kogudus>`__

Kontakt: Ahti Udam, tel 56945978
