.. image:: /../docs/.static/20210807_165825.jpg
  :width: 90%

Tere tulemast Eesti Taizé Sõprade kodulehel.

.. Euroopa Noorte Kokkutulek 2024-25 :doc:`toimus Tallinnas <tallinn2024-25/index>`


..  /../docs/1735sl32-360.jpg
.. /../docs/data/pilgrimage_of_trust.png


.. Covid-19 eriolukorra tõttu jäävad alates 13. märtsist kõik palvused ära.
   Igal õhtul kell 21:30 (Eesti aeg) toimub Taizé vennaskonnas palvus, millel
   saab virtuaalselt osaleda.  Vaata https://www.taize.fr/en_article27540.html


.. rubric:: Iganädalased ühispalvused Eestis

========= ===== ======== ==================================
E         18:00 Tallinn  :doc:`Kaarli kirik <kaarli>`
K         19:00 Tartu    :doc:`Luuka kirik <luuka>`
N         17:30 Tartu    :doc:`Pauluse kirik <pauluse>`
N         19:00 Loksa    :doc:`Loksa kirik <loksa>`
R         19:00 Tallinn  :doc:`Mustamäe kirik <mmmk>`
R         19:00 Räpina   :doc:`Räpina kirik <rapina>`
========= ===== ======== ==================================

Vaata ka: :doc:`Kõik ühispalvused Eestis <prayers>`.


.. .. rubric:: Viimased blogipostitused

.. .. blogger_latest::


Avaleht
=======

.. .. raw:: html

  <script type="text/javascript" src="https://www.taize.fr/tz_readings.php?lang=en"></script>

.. .. rubric:: Sisu

.. toctree::
   :maxdepth: 1

   prayers
   taize
   music
   more
   about


.. toctree::
   :hidden:

   tallinn2024-25/index
   jaga
   kohad
   virufolk
   lsk
   upr
   wip/0309
   wip/0714
   join
   lists
   noored
   korraldajad
   kalender
   palvused
   uudised
   reisid
