=========
Löö kaasa
=========

..    https://eestitaizes6brad.zulipchat.com

..
  Meil on pakkuda järgmisi ametikohti.
  Võta ühendust juhatusega (juhatus@laudate.ee), kui mõni nendest ametikohtadest
  Sind huvitab.


  - **Kontaktihaldaja** hoolitseb kontaktiandmete eest.
    Kontaktipidamine on ka oluline.  On oluline, et me
    tunneme üksteist, et me kuulame inimeste vajadusi ja et me suhtleme omavahel.
  - **Veebiredaktor** paneb sisu üles ja vaatab, et see näeks välja normaalne.
  - **Süsteemiadministraator** paneb server käima ja paigaldab tarkvara.
  - **Autor** küsitleb inimesi, kirjutab artikleid, uudiseid ja kuulutusi.
  - **Moderaator** aitab inimestel meililistides jagada seda, mida nad teevad.
  - **Noodiredaktor** oskab kasutada `MuseScore <https://musescore.org/en>`__ ja hoolitseb noodifailide eest.
