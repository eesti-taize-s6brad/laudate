====================
KOSE kirik
====================

.. grid:: 1 2 2 2

  .. grid-item::


    Palvus Taizé lauludega kord kuus laupäeviti.

    Asukoht : Püha Nikolause kirik, `Kose <https://et.wikipedia.org/wiki/Kose>`__

    Korraldaja: `EELK Kose Püha Nikolause kogudus <https://sites.google.com/site/eelkkose/>`__

    Kontakt: Õpetaja Kerstin Kask, Tel 553 0399, kerstin.kask@eelk.ee

  .. grid-item::

    .. raw:: html

      <a
      title="Kose kirik suvi 2012"
      href="https://commons.wikimedia.org/wiki/File:Kose_kirik_suvi_2012.jpg#/media/Fail:Kose_kirik_suvi_2012.jpg">
       <img
        src="https://upload.wikimedia.org/wikipedia/commons/2/29/Kose_kirik_suvi_2012.jpg"
        alt="Kose kirik suvi 2012.jpg"
        width="90%"/>
      </a>

    (Foto: Ivar Leidus via Wikimedia Commons, `CC BY-SA 3.0 EE <https://creativecommons.org/licenses/by-sa/3.0/ee/deed.en>`__)
