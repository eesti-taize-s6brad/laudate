=======
Kontakt
=======

Veebilehe ``laudate.ee`` vastutav väljaandja on seltsing :doc:`"Eesti Taizé
Sõbrad" <seltsing>`. Meiega suhtlemiseks kirjuta emaili:

- Veebiredaktor: webmaster@laudate.ee (Luc Saffre)
- Juhatus: juhatus@laudate.ee (Tiina Klement, Annely Neame ja Luc Saffre)

**Leidsid vea?**
Kirjuta aadressile webmaster@laudate.ee ja kopeeri seda teksti mis vajab
parandust! Võimalusel tee ka parandusettepanekut.

Vaata ka :doc:`help`.
