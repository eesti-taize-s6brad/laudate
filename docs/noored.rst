========
Noortele
========

Taizé vaimsus ja palvestiil kõnetab eelkõige 14-30-aastaseid noori. Tõsi küll,
Eestis on Taizé sõprade keskmine iga veidi tõusnud. Aga see ei pruugi nii jääda.
Just sellepärast tahame sind julgustada: jaga oma sõpradega seda, mis sulle
meeldib, ja kutsu neid kaasa.

"Tänapäeva noortel on huvitav vaimsus, sest neil on vähem teadmisi religiooni
kohta kui vanematel põlvkondadel. Kristlikus elus polegi nii tähtis teada
*midagi* (oma religiooni kohta) vaid õppida *kedagi* (Jeesust) tundma. Pole nii
tähtis, mida sa "tead" Jumala kohta, vaid see, et sa räägid Temaga ja püüad
Temaga suhelda.  See on midagi, millele noored inimesed on vastuvõtlikud, sest
nad ei ole kinni teadmistes.  Noored on ka väga avatud teistele kultuuridele.
See on suur areng võrreldes sellega, mis oli paar põlvkonda tagasi." (vend
Philip aprillikuus 2016 `Plussmeedia intervjuus
<https://www.youtube.com/watch?v=fF_zRdInK8g>`__, minutist 6:00)

"Taizé on koht, kus noored ruulivad. Nad on KÕIGE tähtsamad, nad ei pea mitte
midagi tõestama, ei pea mõtlema ratsionaalselt ja struktuuridesse ära mahtuma.
Nad võivad olla andekad, ägedad, kiired, tolerantsed, lärmakad, vait, koos,
üksi... Nad võivad omavahel asju arutada ja ära teha... Taizés aumärke ei
jagata, aga seal saab tuhandete teistega kogeda, milline võib olla elus elu,
täis hetki mis voolavad igavikku ja nendes hetkedes tunned, et SELLIST KIRIKUT
MA TAHTSINGI❤️" (Annely Neame)

"Kui püüame end näha üheskoos teel olevate palveränduritena, siis õppime
usaldama ja avama teekaaslastele oma südant ilma kartuse ja umbusuta, pidades
silmas vaid seda, mida tõesti otsime: rahu ühe ja ainsa Jumala juures." (paavst
Franciscuse kõne noortekohtumisel Tallinna Kaarlik kirikus 25. septembril 2018)

.. toctree::
   :maxdepth: 1

   nk
   pv
