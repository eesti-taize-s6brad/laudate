.. raw:: html

  <img width="30%"
  alt="Usalduse palverännak Eestis" align="right"
  src="https://www.laudate.ee/data/2021/IMG_20210710_230449.jpg">

===================
Ühispalvused Eestis
===================


.. contents::
   :depth: 1
   :local:

Kuhu minna?
===========

Regulaarsed :ref:`ühispalvused <common_prayer>` Taizé lauludega toimuvad Eesti
erinevaites paikades:

- TALLINN :
  :doc:`Kaarli <kaarli>` /
  :doc:`Mustamäe <mmmk>` /
  :doc:`Peeteli <peeteli>` /
  :doc:`Pirita <pirita>` /
  :doc:`Pühavaimu <pyhavaimu>`

- TARTU:
  :doc:`pauluse` / :doc:`luuka`

- MUJAL EESTIS:
  :doc:`haapsalu` /
  :doc:`hiiumaal` /
  :doc:`kasmu` /
  :doc:`keila` /
  :doc:`kose` /
  :doc:`loksa` /
  :doc:`noo` /
  :doc:`rapina` /
  :doc:`viljandi` /
  :doc:`saku`


.. toctree::
   :hidden:

   mmmk
   jaani
   kaarli
   peeteli
   pirita
   pyhavaimu
   ristiku
   pauluse
   luuka
   haapsalu
   hiiumaal
   kasmu
   keila
   kose
   loksa
   noo
   rapina
   saku
   viljandi

Vaata ka Taizé vennaskonna kodulehel `Regulaarsed palvused Taizé lauludega
<https://www.taize.fr/et_article26818.html>`__ .



.. _kalender:

Kalender
========

Allolevas Google kalendris on kirjas, mis toimub Eestis seotud Taizéga.

.. raw:: html

    <iframe src="https://calendar.google.com/calendar/embed?src=mgs1hqa30g6qm9cup3p2g4h09c%40group.calendar.google.com&ctz=Europe%2FTallinn"
    style="border: 0" width="100%" height="400" frameborder="0" scrolling="no"></iframe>

.. |add_google|  image:: /../docs/add_calendar.png
    :width: 100px

Ühenda meie kalender oma Google kalendriga, et olla kursis kõikide sündmustega!
(`juhend <https://support.google.com/calendar/answer/37100>`__)


.. Kliki |add_google| ning järgi juhendeid.



.. _common_prayer:

Mis on ühispalvus?
==================

.. sidebar::

  "Me ei pea koos palvetamiseks ootama, et kõik teoloogilised küsimused leiaksid
  üksmeelse vastuse. Kui me kohtume erinevate konfessioonide esindajatega, (...)
  [võime] üha uuesti kõigepealt koos palvetada. Sel viisil kogeme meie juba
  praegu ühtsust ja aitame Jumala rahval liikuda ühtse usutunnistuse poole." --
  vend Alois (`Kiri 2023 <https://www.taize.fr/et_article36189.html>`__)

**Ühispalvus** on palvus, mis ei eelda kindlat usutunnistust ega kuuluvust teatud
kogudusele. Kõik on oodatud, k.a. neid, kes pole harjunud liturgiliste
talitustega.

Selleks, et kõik tunneksid end hästi, kasutatakse ühispalvuses tavaliselt Taizé
vennaskonnas arenenud lihtne ja meditatiivne palvestiil. Palvus kestab reeglina
45 minutit, mille ajal on peamine väljendusviis laulmine. Toimub üksainus lühike
lugemine Piiblist ilma jutluseta.  Palve keskpunktiks on vaikusehetk, mis kestab
kuni 10 minutit. Lapsed võivad palvuse ajal mängida, joonistada ning olla
lihtsalt lapsed.

..
  **Ühispalvus** on palvus :doc:`Taizé vennaskonnas <taize>` arenenud palvestiilis.
  Tavaline ühispalvus kestab umbes 45 minutit. Palvuse ajal ei räägita palju,
  pea­mine väljendusviis on laul. Millalgi on lühike lugemine Piiblist ilma
  jutluseta. Pärast seda oleme 5-10 minutit täie­likuses vaikuses.

..
  Seda iseloomustavad
  - korduvad meditatiivsed ja mitme­häälsed koorilaulud,
  - piibliteksti lugemine mitmes keeles ilma jutluseta ja
  - üks üsna pikk vaikuse hetk.




..
  Ühispalvus on **interkonfessionaalne**, s.t. igaüks on teretulnud.
  Me näeme vaeva selleks, et igaüks saaks ennast tunda hästi.
  Sa ei pea olema "kirikuinimene". Sa ei pea sele­tama, miks sa tulid.
  Lapsed on eriti tere­tulnud, nad tohivad palvuse ajal mängida või joonistada.

.. kõik konfessioonid (luterlased,
  katoliiklased, õigeusklikud, baptistied, metodistid, nelipühilased, adventistid
  jne) on teretulnud osaleda. Ja veel enam: teretulnud on ka inimesed, kes ei pea
  ennast "usklikuks".


.. Palvestiil arenes eriti noori inimesi silma pidades, aga ka vanemad inimesed on lubatud.

Taizé laulud koosnevad tihti vaid ühest lausest või fraasist, mida korratakse
meditatiivselt. Laulude seaded on mitmehäälsed (SATB) ning noodid jagatakse
kõigile osalejatele, kõik on oodatud laulma, kas noodist või kuulmise järgi
vastavalt oma võimekusele ning häälerühmale. Laulutekstide aluseks on Piibel või
tekstid varakristlikelt autoritelt.

..
  Taizé laulud koosnevad tihti ainult ühest lausest ja neid korratakse nii kaua,
  et ka vähem andekas laulja saab mõne aja pärast kaasa laulda. Laulud on enamasti
  SATB seades, kõik laulude noodid on iga osaleja käes. Kes noote oskab lugeda, on
  kutsutud oma häält laulda. Laulude tekstid on võetud Piiblist või kirikuisadest.


Tähelepanekud palvuse korraldajatele
====================================

Sa soovid korraldada palvust Taizé stiilis oma koguduses? Väga tore!
Siin on mõned tähelepanekud.

Avalikus ruumis soovitame kasutada väljendit "Palvus Taizé lauludega" või
lihtsalt :ref:`ühispalvus <common_prayer>`. Väljend "Taizé palvus" on küll
mugavam, aga see võib tekitada vale mulje, nagu oleks olemas mõni kindel
palvekord, mida Taizé vennaskond määraks ette. See pole nii. Iga korraldaja on
kutsutud ise mõtlema, kuidas täpselt tema palvused toimuvad; vennaskond jagab
vaid oma kogemusi ja soovitusi.

Loe ka Taizé kodulehelt teemal *Kuidas palvust ette valmistada?*:

- `Palvuse ettevalmistamine <https://www.taize.fr/et_article857.html>`__
- `Paiga ettevalmistamine ühispalvuseks <https://www.taize.fr/et_article887.html>`__
- `Ikoonid <https://www.taize.fr/et_article888.html>`__

Taizé vennad tegid ka videot inglise keeles:

- https://www.youtube.com/watch?v=fphe1RuD5UM

Soovitame jagada kohalikele inimestele konkreetseid ülesandeid. Need ülesanded
võivad olla heaks "ettekäändeks" noori inimesi leida ja neid palvusele kutsuda.
Mõned "ametikohad":

- **Tervitaja** räägib paar sõna iga sisenejaga. Ta ei häbene juttu alustada. Ta
  vastab küsimustele ja küsib omakorda: "Kas olete juba Taizé palvusel olnud?".
  Ta jagab inimestele lauluraamatuid (ja selgitab, et neid pole
  kaasavõtmiseks...)

- Taizés loetakse piiblitekste mitmes keeltes sellepärast, et kuulmine on
  otsesem kanal kui lugemine. Kui tuleks mõni mitteeestlane, siis võiks
  tervitaja küsida, kas ta oleks nõus piiblit oma emakeeles ette lugema. Ja sel
  juhul peaks olema keegi, kes oskab **oma telefoniga internetist erinevates
  keeltes piiblitekste leida** ja kes siis lugemise ajaks oma telefoni laenab.

- **Kohanäitaja** on inimestele abiks koha leidmisel, nihutab vajadusel toole...
  Inimesed ju kardavad üldiselt avalikel üritustel "ettepoole" minna. See on
  loomulik ja arusaadav eriti koroona ajal, ent samal ajal on Jumala lähedus
  kogetav ka selle kaudu, et sa oled füüsiliselt altari ja teiste inimeste
  lähedal. Kohanäitaja on inimene, kes seda mõistab ja jälgib, et kõigil oleks
  hea olla.

Failid allalaadimiseks
===========================

Kui tahad ise midagi muuta, siis võid odt faili kasutada.


- Flaier "Tere tulemast", mille võiks uuele külalisele anda:
  `pdf <https://laudate.ee/data/welcome.pdf>`__ /`odt <https://laudate.ee/data/welcome.odt>`__
- Mida võiks öelda sissejuhatuseks:
  `pdf <https://www.laudate.ee/data/2021/ViruFolk2021tere.pdf>`__ / `odt <https://www.laudate.ee/data/2021/ViruFolk2021tere.odt>`__

- Pitser "Pilgrimage of Trust in Estonia" 3 keeles (et, en, ru) ja QR viitega:
  `png <https://laudate.ee/data/pilgrimage_of_trust.png>`__/
  `odt <https://laudate.ee/data/pilgrimage_of_trust.odt>`__.
  Kui teed plakatit või prindid midagi välja, siis võid sellega
  väljendada, et võtad osa usalduse palverännakust.

..
  - Juhtnöörid Noortekohtumiste korraldajatele
    `pdf <https://www.laudate.ee/data/nk-korraldajale.pdf>`__ /
    `odt <https://www.laudate.ee/data/nk-korraldajale.odt>`__.
