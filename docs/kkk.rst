==========================
Korduma kippuvad küsimused
==========================

.. |e12nupp|  image:: e12nupp.png
  :class: pi-button
  :alt: kriipsudega klahv


Kuidas hääldatkse "Taizé"?
==========================

Seda hääldatakse "tezee" või "täzee". Tegelikult on esimese silbi täishäälik
eesti helikute "e" ja "ä" vahel, lihtsalt üsna ä-ine e või üsna e-ine ä.


Kuidas kirjutada eesti klaviatuuril "Taizé"?
============================================

Ülemisel paremal nurgal asub klahv, millel on ainult kaks kriipsu: |e12nupp|.
Sellele klahvile vajutad ja lased lahti. Midagi pole näha, sest see on *tumm*
klahv. Nüüd vajutad "e" klahvile. Tulemus peaks olema "é".

Ettevaatust: kui vajutad |e12nupp| klahvile, siis ära vajuta samal ajal ka
"Shift" klahvile. Sest siis on tulemus "è" ja "è" on prantsuse keeles hoopis
midagi muu kui "é"!

.. (Pildi allikas: `eki.ee <http://www.eki.ee/itstandard/2000/klaviatuur.shtml>`__)
