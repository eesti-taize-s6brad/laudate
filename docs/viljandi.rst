====================
VILJANDI Jaani kirik
====================

.. sidebar::

    .. raw:: html

      <p align="center"><a title="Saint John's church, Tallinn, Estonia, 2012-08-05"
      href="https://commons.wikimedia.org/wiki/File:Viljandi_Jaani_kirik2.jpg"><img
      width="100%"
      alt="Viljandi Jaani kirik"
      src="https://upload.wikimedia.org/wikipedia/commons/d/dd/Viljandi_Jaani_kirik2.jpg"></a></p>

    Viljandi Jaani kirik (Foto: Ivar Leidus, `CC BY-SA 3.0 EE
    <https://creativecommons.org/licenses/by-sa/3.0/ee/deed.en>`__, via
    Wikimedia Commons)


Palvus Taizé lauludega iga kuu viimase pühapäeval kl 19:00.

Asukoht : `Viljandi Jaani kirik
<https://et.wikipedia.org/wiki/Viljandi_Jaani_kirik>`__ (Pikk tänav 6)

Korraldaja: `EELK Viljandi Jaani kogudus <https://viljandi.jaani.eelk.ee/>`__

| Kontakt:
| Koguduse õpetaja Marko Tiitus, tel 5341 3394
| EAÕK preester Kristjan (Otsmann), tel 502 9037
