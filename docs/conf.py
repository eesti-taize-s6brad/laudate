# -*- coding: utf-8 -*-

extensions = []
templates_path = []

# html_theme = "sphinx_rtd_theme"
# html_theme = "alabaster"

from atelier.sphinxconf import configure ; configure(globals())

extensions += ['rstgen.sphinxconf.blog']

extensions += ['rstgen.sphinxconf.sigal_image']
sigal_base_url = 'https://sigal.saffre-rumma.net'

extensions += ['sphinxcontrib.youtube']
# extensions += ['sphinxcontrib.yt']

# sigal_base_url = 'http://sigal.saffre-rumma.net'


# General substitutions.
project = 'Laudate Dominum'
import datetime
copyright = '2020-{} Eesti Taizé Sõbrad'.format(datetime.date.today().year)

language = 'et'

# The name for this set of Sphinx documents.  If None, it defaults to
# "<project> v<release> documentation".
# html_title = "Eesti Taizé Sõbrad"
html_title = project

# A shorter title for the navigation bar.  Default is the same as html_title.
# html_short_title = "Kodu"

# The name of an image file (relative to this directory) to place at the top
# of the sidebar.
# html_logo = "logo2.jpg"
# html_logo = "logo.png"
# html_logo = "1735sl32-360.jpg"
# html_logo = "20210807_165825.jpg"
# html_logo = str(Path('../docs/.static/20210807_165825.jpg').resolve()) # pandas
# html_logo = str(Path('../docs/data/pilgrimage_of_trust.png').resolve()) # pandas

if html_theme == "insipid":
    html_context.update({
        'display_gitlab': True,
        'gitlab_user': 'eesti-taize-s6brad',
        'gitlab_repo': 'laudate',
    })

# if html_theme == "sphinx_rtd_theme":
#
#     html_sidebars = {
#         # '**': ['select_lang.html', 'searchbox.html'],
#         '**': ['globaltoc.html', 'languages.html', 'searchbox.html', 'links.html'],
#     }


# Additional templates that should be rendered to pages, maps page names to
# template names.
#html_additional_pages = {}

# If false, no module index is generated.
html_use_modindex = False

# If false, no index is generated.
html_use_index = False

# If true, the index is split into individual pages for each letter.
#html_split_index = False

# If true, the reST sources are included in the HTML build as _sources/<name>.
#html_copy_source = True

# If true, an OpenSearch description file will be output, and all pages will
# contain a <link> tag referring to it.  The value of this option must be the
# base URL from which the finished HTML is served.
#html_use_opensearch = ''

# If nonempty, this is the file name suffix for HTML files (e.g. ".xhtml").
#html_file_suffix = ''

# Output file base name for HTML help builder.
# htmlhelp_basename = 'laudate'


# Options for LaTeX output
# ------------------------

# The paper size ('letter' or 'a4').
#latex_paper_size = 'letter'

# The font size ('10pt', '11pt' or '12pt').
#latex_font_size = '10pt'

# Grouping the document tree into LaTeX files. List of tuples
# (source start file, target name, title, author, document class [howto/manual]).
# latex_documents = [
#   ('index', 'saffre-rumma.tex', u'saffre-rumma',
#    u'saffre-rumma', 'manual'),
# ]

# The name of an image file (relative to this directory) to place at the top of
# the title page.
#latex_logo = None

# For "manual" documents, if this is true, then toplevel headings are parts,
# not chapters.
#latex_use_parts = False

# Additional stuff for the LaTeX preamble.
#latex_preamble = ''

# Documents to append as an appendix to all manuals.
#latex_appendices = []

# If false, no module index is generated.
#latex_use_modindex = True


extensions += ['sphinxfeed']
feed_base_url = 'https://www.laudate.ee'
feed_author = 'Eesti Taizé Sõbrad'
feed_title = "Laudate Dominum"
feed_field_name = 'date'
feed_description = "Eesti Taizé Sõprade veebileht"

# extensions += ['hieroglyph']




# https://github.com/sphinx-doc/sphinx/issues/3806
# https://github.com/sphinx-doc/sphinx/issues/3788#issuecomment-304234520
# Thanks to Jean-François B.

# import docutils
# from docutils.languages import en
#
# def patched_get_language(language_code, reporter=None):
#     return en
#
# def setup(app):
#     docutils.languages.get_language = patched_get_language


# html_theme="alabaster"
# my_font_family = "Swiss, Helvetica, 'Liberation Sans'"
# html_theme_options={
#     "font_family": my_font_family,
#     "head_font_family": my_font_family,
# }

if html_theme == "sphinx_rtd_theme":
    html_theme_options={
        "prev_next_buttons_location": "none",
        "style_nav_header_background": "#dddddd",
        "style_external_links": False,  # disadvantage: line spacing increases for lines with a link
        "includehidden": False,
        "display_version": False,
        'navigation_depth': 2,
    }


html_show_sourcelink = False
html_docstitle = ""
html_show_sphinx = False


# templates_path.append(".templates")

templates_path.insert(0, str(Path("../docs/.templates").resolve()))


if html_theme == "insipid":
    html_theme_options = {
        # 'body_max_width': None,
        # 'breadcrumbs': True,
        'globaltoc_includehidden': False,
        'left_buttons': [
            'search-button.html',
            'home-button.html',
        ],
        'right_buttons': [
            'languages-button.html',
            'fullscreen-button.html',
            # 'repo-button.html',
            'facebook-button.html'
        ],
    }

html_sidebars = { '**' : []}


# if html_theme == "insipid":
#     html_theme_options = {
#         # 'body_max_width': None,
#         # 'breadcrumbs': True,
#         'globaltoc_includehidden': False,
#         'left_buttons': [
#             'search-button.html',
#             'home-button.html',
#             'languages-button.html',
#         ],
#         'right_buttons': [
#             'fullscreen-button.html',
#             # 'repo-button.html',
#             'facebook-button.html',
#         ],
#     }
#     html_sidebars = {
#         '**' : [
#             #'languages.html',
#             'globaltoc.html',
#             'separator.html',
#             'searchbox.html',
#             'indices.html',
#             'links.html'
#             ]}

rst_prolog = """
:doc:`Avaleht </index>` |
:doc:`Palvused </prayers>` |
:doc:`/music` |
:doc:`Taizé </taize>` |
:doc:`/more` |
:doc:`/about`

"""

# :doc:`Kokkutulek 2024/25 </tallinn2024-25/index>` |
