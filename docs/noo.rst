=========
NÕO kirik
=========

.. grid:: 1 2 2 2

  .. grid-item::


    Palvused reeglina iga kuu kolmandal reedel kell 19:30

    Asukoht: `Nõo Püha Laurentsiuse kirik <https://et.wikipedia.org/wiki/N%C3%B5o_kirik>`__ (Tartu tänav 2, Nõo alevik, 61601 Nõo vald, Tartumaa)

    Korraldaja: `EELK Nõo Püha Laurentsiuse kogudus <http://nookirik.ee/teated/>`__

    Kontakt: Õpetaja Mart Jaanson, tel 5661 1443, mart.jaanson@eelk.ee

  .. grid-item::

    .. raw:: html

      <p><a href="https://commons.wikimedia.org/wiki/File:N%C3%B5o_church_2008_22.jpg#/media/Fail:Nõo_church_2008_22.jpg">
      <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/3/3b/N%C3%B5o_church_2008_22.jpg/320px-N%C3%B5o_church_2008_22.jpg"
      alt="Nõo church 2008 22.jpg" width="320" height="240">
      </a>

    (Foto: Hendrixeesti via Wikimedia Commons, Üleslaadija oma töö, Avalik omand)
