================================
Pirita kloostri kabel
================================

.. .. raw:: html

  <a title="Luc Saffre, CC BY-SA 4.0
  &lt;https://creativecommons.org/licenses/by-sa/4.0&gt;, via Wikimedia Commons"
  href="https://commons.wikimedia.org/wiki/File:Mustam%C3%A4e_Maarja_Magdaleena_kirik_(tagant,_Aprill_2021).jpg"><img
  width="40%" align="right"
  alt="Mustamäe Maarja Magdaleena kirik (tagant, Aprill 2021)"
  src="https://upload.wikimedia.org/wikipedia/commons/thumb/6/66/Mustam%C3%A4e_Maarja_Magdaleena_kirik_%28tagant%2C_Aprill_2021%29.jpg/512px-Mustam%C3%A4e_Maarja_Magdaleena_kirik_%28tagant%2C_Aprill_2021%29.jpg"></a>

Palvus Taizé lauludega iga 28. kuupäeval kell 18:00.

.. Enne palvust kell 17.30-18.30 :doc:`lauluklubi <lauluproovid>`.

Asukoht : Merivälja tee 18, 11911 Tallinn

.. Korraldaja: ...

Kontakt: Silja Trisberg (Tel 5208481)
